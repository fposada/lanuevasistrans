package rest;

import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import tm.RotondAndesTM;
import vos.Usuario;

@Path("{idUsuario}/consultas")
public class RotondAndesConsultas {

		/**
		 * Atributo que usa la anotacion @Context para tener el ServletContext de la conexion actual.
		 */
		@Context
		private ServletContext context;

		/**
		 * Metodo que retorna el path de la carpeta WEB-INF/ConnectionData en el deploy actual dentro del servidor.
		 * @return path de la carpeta WEB-INF/ConnectionData en el deploy actual.
		 */
		private String getPath() {
			return context.getRealPath("WEB-INF/ConnectionData");
		}


		private String doErrorMessage(Exception e){
			return "{ \"ERROR\": \""+ e.getMessage() + "\"}" ;
		}

		@GET
		@Path("RFC10/{fechaIn}/{fechaFi}/{nomRes}/{nomPro}/{nomTipo}")
		@Produces({ MediaType.APPLICATION_JSON })
		public Response llenarMenus(@PathParam("idUsuario") String idUsuario,@PathParam("fechaIn") String fechaIn,@PathParam("fechaFi") String fechaFi,@PathParam("nomRes") String nomRes,@PathParam("nomPro") String nomPro,@PathParam("nomTipo") String nomTipo) {
			List<Usuario> res;
			RotondAndesTM tm = new RotondAndesTM(getPath());
			try {
				res=tm.RFC10(idUsuario, fechaIn, fechaFi, nomRes, nomPro, nomTipo);
			} catch (Exception e) {
				return Response.status(500).entity(doErrorMessage(e)).build();
			}
			return Response.status(200).entity(res).build();
		}

		@GET
		@Path("clientesBuenos")
		@Produces({ MediaType.APPLICATION_JSON })
		public Response llenarProdPed(@PathParam("idUsuario") String idUsuario) {
			List<Usuario> lis;
			
			RotondAndesTM tm = new RotondAndesTM(getPath());
			try {
				 lis=tm.darClientesBuenos(idUsuario);
			} catch (Exception e) {
				return Response.status(500).entity(doErrorMessage(e)).build();
			}
			return Response.status(200).entity(lis).build();
		}


		


	}
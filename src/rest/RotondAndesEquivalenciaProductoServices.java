package rest;

import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.map.ObjectMapper;

import tm.RotondAndesTM;
import vos.EquivalenciaIngredientePedido;
import vos.EquivalenciaProducto;
import vos.EquivalenciaProductoPedido;


@Path("{idUsuario}/EquivalenciaProductos")
public class RotondAndesEquivalenciaProductoServices {

	/**
	 * Atributo que usa la anotacion @Context para tener el ServletContext de la conexion actual.
	 */
	@Context
	private ServletContext context;

	/**
	 * Metodo que retorna el path de la carpeta WEB-INF/ConnectionData en el deploy actual dentro del servidor.
	 * @return path de la carpeta WEB-INF/ConnectionData en el deploy actual.
	 */
	private String getPath() {
		return context.getRealPath("WEB-INF/ConnectionData");
	}


	private String doErrorMessage(Exception e){
		return "{ \"ERROR\": \""+ e.getMessage() + "\"}" ;
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getEquivalenciaProductos() {
		RotondAndesTM tm = new RotondAndesTM(getPath());
		List<EquivalenciaProducto> EquivalenciaProductos;
		try {
			EquivalenciaProductos = tm.darEquivalenciasProducto();
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(EquivalenciaProductos).build();
	}

	@GET
	@Path( "{nameP}/{nameR}" )
	@Produces( { MediaType.APPLICATION_JSON } )
	public Response getEquivalenciaProducto( @PathParam( "nameP" ) String nameP, @PathParam("nameR") String nameR )
	{
		List<EquivalenciaProducto> v;
		RotondAndesTM tm = new RotondAndesTM( getPath( ) );
		try
		{
			v = tm.darEquivalenciasProductoPorProductoEnRestaurante(nameP, nameR);
			return Response.status( 200 ).entity( v ).build( );			
		}
		catch( Exception e )
		{
			return Response.status( 500 ).entity( doErrorMessage( e ) ).build( );
		}
	}


	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addEquivalenciaProducto(@PathParam("idUsuario") String idUsuario,EquivalenciaProducto EquivalenciaProducto) {
		RotondAndesTM tm = new RotondAndesTM(getPath());
		try {
			tm.addEquivalenciaProducto(idUsuario,EquivalenciaProducto);
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(EquivalenciaProducto).build();
	}
	
	@POST
	@Path("/enPedido")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addEquivalenciaIngredientePedido(@PathParam("idUsuario") String idUsuario,EquivalenciaProductoPedido EquivalenciaIngrediente) {
		RotondAndesTM tm = new RotondAndesTM(getPath());
		try {
			tm.addEquivalenciaProductoPedido(idUsuario,EquivalenciaIngrediente);
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(EquivalenciaIngrediente).build();
	}


//	@PUT
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Produces(MediaType.APPLICATION_JSON)
//	public Response updateEquivalenciaProducto(@PathParam("idUsuario") String idUsuario,EquivalenciaProducto EquivalenciaProducto) {
//		RotondAndesTM tm = new RotondAndesTM(getPath());
//		try {
//			tm.updateEquivalenciaProducto(idUsuario,EquivalenciaProducto);
//		} catch (Exception e) {
//			return Response.status(500).entity(doErrorMessage(e)).build();
//		}
//		return Response.status(200).entity(EquivalenciaProducto).build();
//	}


	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteEquivalenciaProducto(@PathParam("idUsuario") String idUsuario,EquivalenciaProducto EquivalenciaProducto) {
		RotondAndesTM tm = new RotondAndesTM(getPath());
		try {
			tm.deleteEquivalenciaProducto(idUsuario,EquivalenciaProducto);
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(EquivalenciaProducto).build();
	}
}



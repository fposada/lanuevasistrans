package tm;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import java.text.ParseException;

import dao.DAORFC11;
import dao.DAORFC7;
import dao.DAORFC8;
import dao.DAORFC9;
import dao.DAOTablaEquivalenciaIngrediente;
import dao.DAOTablaEquivalenciaIngredientePedido;
import dao.DAOTablaEquivalenciaProducto;
import dao.DAOTablaEquivalenciaProductoPedido;
import dao.DAOTablaIngredientes;
import dao.DAOTablaMesa;
import dao.DAOTablaPedidos;
import dao.DAOTablaPreferencias;
import dao.DAOTablaProductoIngrediente;
import dao.DAOTablaProductoMenu;
import dao.DAOTablaProductoPedido;
import dao.DAOTablaProductos;
import dao.DAOTablaRestauranteProducto;
import dao.DAOTablaRestaurantes;
import dao.DAOTablaUsuarios;
import dao.DAOTablaZonas;
import vos.EquivalenciaIngrediente;
import vos.EquivalenciaIngredientePedido;
import vos.EquivalenciaProducto;
import vos.EquivalenciaProductoPedido;
import vos.FechaInicial;
import vos.FechaNombreCuenta;
import vos.InfoUsuario;
import vos.InfoZonaInfoPedido;
import vos.Ingrediente;
import vos.Mesa;
import vos.Pedido;
import vos.Preferencia;
import vos.Producto;
import vos.ProductoIngrediente;
import vos.ProductoMenu;
import vos.ProductoPedido;
import vos.RFC1;
import vos.RFC11FechaProducto;
import vos.RFC11FechaRestaurante;
import vos.RFC2;
import vos.RFC7;
import vos.RFC8Consolidador;
import vos.RFC8NoRegistrado;
import vos.RFC8Registrado;
import vos.RFC9Simple;
import vos.Restaurante;
import vos.RestauranteFecha;
import vos.RestauranteProducto;
import vos.Usuario;
import vos.Zona;

public class RotondAndesTM {

	/**
	 * Atributo estatico que contiene el path relativo del archivo que tiene los datos de la conexion
	 */
	private static final String CONNECTION_DATA_FILE_NAME_REMOTE = "/conexion.properties";

	/**
	 * Atributo estatico que contiene el path absoluto del archivo que tiene los datos de la conexion
	 */
	private  String connectionDataPath;

	/**
	 * Atributo que guarda el usuario que se va a usar para conectarse a la base de datos.
	 */
	private String user;

	/**
	 * Atributo que guarda la clave que se va a usar para conectarse a la base de datos.
	 */
	private String password;

	/**
	 * Atributo que guarda el URL que se va a usar para conectarse a la base de datos.
	 */
	private String url;

	/**
	 * Atributo que guarda el driver que se va a usar para conectarse a la base de datos.
	 */
	private String driver;

	/**
	 * conexion a la base de datos
	 */
	private Connection conn;


	/**
	 * Metodo constructor de la clase RotondAndesTM, esta clase modela y contiene cada una de las 
	 * transacciones y la logica de negocios que estas conllevan.
	 */
	public RotondAndesTM(String contextPathP) {
		connectionDataPath = contextPathP + CONNECTION_DATA_FILE_NAME_REMOTE;
		initConnectionData();
	}

	/**
	 * Metodo que  inicializa los atributos que se usan para la conexion a la base de datos.
	 * <b>post: </b> Se han inicializado los atributos que se usan par la conexion a la base de datos.
	 */
	private void initConnectionData() {
		try {
			File arch = new File(this.connectionDataPath);
			Properties prop = new Properties();
			FileInputStream in = new FileInputStream(arch);
			prop.load(in);
			in.close();
			this.url = prop.getProperty("url");
			this.user = prop.getProperty("usuario");
			this.password = prop.getProperty("clave");
			this.driver = prop.getProperty("driver");
			Class.forName(driver);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Metodo que  retorna la conexion a la base de datos
	 * @return Connection - la conexion a la base de datos
	 * @throws SQLException - Cualquier error que se genere durante la conexion a la base de datos
	 */
	private Connection darConexion() throws SQLException {
		System.out.println("Connecting to: " + url + " With user: " + user);
		return DriverManager.getConnection(url, user, password);
	}

	public List<Usuario> darUsuarios(String idUs) throws Exception {
		List<Usuario> Usuarios;
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{			
			Usuario u;
			if(idUs.equals("0")) 
			{
				throw new Exception("S�lo un administrador de RotondAndes puede ver todos los usuarios");
			}
			else {
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				u=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!u.getRol().equals("UsuarioAdmin")) {
					throw new Exception("El identificador "+idUs+" no corresponde a un administrador");
				}
			}

			this.conn = darConexion();
			daoUsuarios.setConn(conn);
			Usuarios = daoUsuarios.darUsuarios();
			System.out.println("Hay "+Usuarios.size()+" usuarios");

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoUsuarios.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return Usuarios;
	}
	
	public List<Usuario> darClientesBuenos(String idUs) throws Exception {
		List<Usuario> Usuarios;
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{			
			Usuario u;
			if(idUs.equals("0")) 
			{
				throw new Exception("S�lo un administrador de RotondAndes puede ver la informacion");
			}
			else if(!idUs.equals("idAdmin")) {				
					throw new Exception("El identificador "+idUs+" no corresponde a un administrador");
				
			}

			this.conn = darConexion();
			daoUsuarios.setConn(conn);
			long startTime = System.currentTimeMillis();
			Usuarios = daoUsuarios.darClientesBuenos();
			long stopTime = System.currentTimeMillis();			
			Long tt = stopTime - startTime;;
			String tiempo = "El tiempo de consulta fue: " + tt + "ms";
			String log = "CONSULTA: RFC12 \n" + "TIPO: Restaurante menos frecuentado \n" + "TIEMPO: " + tiempo;
			System.out.println(log);

				return Usuarios;

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoUsuarios.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}
	
	public List<Usuario> RFC10(String idUs, String fechaIn, String fechaFi,String nomRes, String nomPro, String nomTipo) throws Exception {
		List<Usuario> Usuarios;
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		DAOTablaRestaurantes daoRes= new DAOTablaRestaurantes();
		try 
		{			
			Restaurante u;
			if(idUs.equals("0")) 
			{
				throw new Exception("S�lo un administrador de RotondAndes puede ver la informaci�n");
			}
			else if(!idUs.equals("idAdmin")) {
				this.conn=darConexion();
				daoRes.setConn(conn);
				u=buscarRestaurantePorName(nomRes);			
				if(!u.getRepresentante().equals(idUs)) {
					throw new Exception("El identificador "+idUs+" no corresponde al representante del restaurante "+nomRes+" ni a un administrador.");
				}
			}
			System.out.println(fechaIn);
			System.out.println(fechaFi);
			String dia=fechaIn.charAt(0)+""+fechaIn.charAt(1);
			System.out.println(dia);
			String mes=fechaIn.charAt(2)+""+fechaIn.charAt(3);
			String anio=fechaIn.charAt(4)+""+fechaIn.charAt(5)+""+fechaIn.charAt(6)+""+fechaIn.charAt(7);
			String fIn=dia+"-"+mes+"-"+anio;
			
			String diaF=fechaFi.charAt(0)+""+fechaFi.charAt(1);
			String mesF=fechaFi.charAt(2)+""+fechaFi.charAt(3);
			String anioF=fechaFi.charAt(4)+""+fechaFi.charAt(5)+""+fechaFi.charAt(6)+""+fechaFi.charAt(7);
			String fFin=diaF+"-"+mesF+"-"+anioF;



			this.conn = darConexion();
			daoUsuarios.setConn(conn);

			if(nomPro.equals("0")&&!nomTipo.equals("0")) {
				
				long startTime = System.currentTimeMillis();
				Usuarios = daoUsuarios.darUsuariosNoRestTipo(fIn, fFin, nomRes, nomTipo);
				long stopTime = System.currentTimeMillis();			
				Long tt = stopTime - startTime;;
				String tiempo = "El tiempo de consulta fue: " + tt + "ms";
				String log = "CONSULTA: RFC10 \n" + "TIPO: Restaurante menos frecuentado \n" + "TIEMPO: " + tiempo;
				System.out.println(log);
				return Usuarios;
			}
			else if(!nomPro.equals("0")&&nomTipo.equals("0")) {
				long startTime = System.currentTimeMillis();
				Usuarios=daoUsuarios.darUsuariosNoRestPro(fIn, fFin, nomRes, nomPro);
				long stopTime = System.currentTimeMillis();			
				Long tt = stopTime - startTime;;
				String tiempo = "El tiempo de consulta fue: " + tt + "ms";
				String log = "CONSULTA: RFC10 \n" + "TIPO: Restaurante menos frecuentado \n" + "TIEMPO: " + tiempo;
				System.out.println(log);

				return Usuarios;
			}
			else if(nomPro.equals("0")&&nomTipo.equals("0")) {
				long startTime = System.currentTimeMillis();
				Usuarios=daoUsuarios.darUsuariosNoRest(fIn, fFin, nomRes);
				long stopTime = System.currentTimeMillis();			
				Long tt = stopTime - startTime;;
				String tiempo = "El tiempo de consulta fue: " + tt + "ms";
				String log = "CONSULTA: RFC10 \n" + "TIPO: Restaurante menos frecuentado \n" + "TIEMPO: " + tiempo;
				System.out.println(log);

				return Usuarios;
			}
			else
				throw new Exception("Debe ingresar el nombre de un restaurante y alguno de las dos: Un producto o un tipo de producto (Ej: Casero).");

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoUsuarios.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		
	}

	public Usuario buscarUsuarioPorId(String idUs, String id) throws Exception {
		Usuario Usuario=null;
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{
			//////transaccion
			Usuario u;
			if(idUs.equals("0")) 
			{
				throw new Exception("Un usuario no registrado no puede ver la informacion de otros usuarios.");
			}
			else {
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				u=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!u.getRol().equals("UsuarioAdmin"))
				{
					if(!idUs.equals(id)) {
						throw new Exception("S�lo un administrador de RotondAndes o el usuario con id "+id+" puede consultar informacion de este usuario");
					}
				}
			}

			this.conn = darConexion();
			daoUsuarios.setConn(conn);
			Usuario = daoUsuarios.buscarUsuarioPorId(id);

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoUsuarios.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return Usuario;
	}

	public void addUsuario(String idUs, Usuario uzer) throws Exception {
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{
			//////transaccion
			if(uzer.getRol().equals("UsuarioCliente"))
			{
				Usuario u;
				if(idUs.equals("0")) 
				{
					throw new Exception("S�lo un administrador de RotondAndes puede inscribir un cliente nuevo");
				}
				else {
					this.conn=darConexion();
					daoUsuarios.setConn(conn);
					u=daoUsuarios.buscarUsuarioPorId(idUs);			
					if(!u.getRol().equals("UsuarioAdmin")) {
						throw new Exception("El identificador "+idUs+" no corresponde a un administrador");
					}
				}
			}			

			this.conn = darConexion();
			daoUsuarios.setConn(conn);
			daoUsuarios.addUsuario(uzer);
			conn.commit();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoUsuarios.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}
	
	public void addUsuario(Usuario uzer) throws Exception {
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{
			this.conn = darConexion();
			daoUsuarios.setConn(conn);
			daoUsuarios.addUsuario(uzer);
			conn.commit();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoUsuarios.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public void updateUsuario(String idUs, Usuario uzer) throws Exception {
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{
			//////transaccion

			Usuario u;
			if(idUs.equals("0")) 
			{
				throw new Exception("Un usuario no registrado no puede editar la informacion de otros usuarios.");
			}
			else {
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				u=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!u.getRol().equals("UsuarioAdmin"))
				{if(!idUs.equals(uzer.getIdentificacion())) {
					throw new Exception("S�lo un administrador de RotondAndes o el usuario con id "+uzer.getIdentificacion()+" puede editar informacion de este usuario");
				}
				}
			}
			if(buscarUsuarioPorId(idUs, uzer.getIdentificacion())==null)
			{
				throw new Exception ("El usuario no existe actualmente.");
			}
			this.conn = darConexion();
			daoUsuarios.setConn(conn);
			daoUsuarios.updateUsuario(uzer);
			conn.commit();


		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoUsuarios.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public void deleteUsuario(String idUs, Usuario uzer) throws Exception {
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{
			//////transaccion
			Usuario u;
			if(idUs.equals("0")) 
			{
				throw new Exception("Un usuario no registrado no puede eliminar la informacion de otros usuarios.");
			}
			else {
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				u=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!u.getRol().equals("UsuarioAdmin"))
				{if(!idUs.equals(uzer.getIdentificacion())) {
					throw new Exception("S�lo un administrador de RotondAndes o el usuario con id "+uzer.getIdentificacion()+" puede eliminar informacion de este usuario");
				}
				}
			}
			if(buscarUsuarioPorId(idUs, uzer.getIdentificacion())==null)
			{
				throw new Exception ("El usuario no existe actualmente.");
			}
			this.conn = darConexion();
			daoUsuarios.setConn(conn);
			daoUsuarios.deleteUsuario(uzer);
			conn.commit();


		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoUsuarios.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public List<Restaurante> darRestaurantes() throws Exception {
		List<Restaurante> Restaurantes;
		DAOTablaRestaurantes daoRestaurantes = new DAOTablaRestaurantes();
		try 
		{			
			this.conn = darConexion();
			daoRestaurantes.setConn(conn);
			Restaurantes = daoRestaurantes.darRestaurantes();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoRestaurantes.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return Restaurantes;
	}

	public Restaurante buscarRestaurantePorName(String name) throws Exception {
		Restaurante Restaurante=null;
		DAOTablaRestaurantes daoRestaurantes = new DAOTablaRestaurantes();
		try 
		{
			//////transaccion
			this.conn = darConexion();
			daoRestaurantes.setConn(conn);
			Restaurante = daoRestaurantes.buscarRestaurantePorName(name);

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoRestaurantes.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return Restaurante;
	}

	public void addRestaurante(String idUs, Restaurante Restaurante) throws Exception {
		DAOTablaRestaurantes daoRestaurantes = new DAOTablaRestaurantes();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{
			//////transaccion

			Usuario u;
			if(idUs.equals("0")) 
			{
				throw new Exception("S�lo un administrador de RotondAndes puede agregar un restaurante.");
			}
			else {
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				u=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!u.getRol().equals("UsuarioAdmin")) {
					throw new Exception("El identificador "+idUs+" no corresponde a un administrador");
				}
			}			

			this.conn = darConexion();
			daoRestaurantes.setConn(conn);
			daoRestaurantes.addRestaurante(Restaurante);
			conn.commit();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoRestaurantes.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}
	
	public void addRestaurante(Restaurante Restaurante) throws Exception {
		DAOTablaRestaurantes daoRestaurantes = new DAOTablaRestaurantes();
		try 
		{			
			this.conn = darConexion();
			daoRestaurantes.setConn(conn);
			daoRestaurantes.addRestaurante(Restaurante);
			conn.commit();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoRestaurantes.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public void updateRestaurante(String idUs,Restaurante Restaurante) throws Exception {
		DAOTablaRestaurantes daoRestaurantes = new DAOTablaRestaurantes();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();

		try 
		{

			Usuario u;
			if(idUs.equals("0")) 
			{
				throw new Exception("S�lo un usuarioRestaurante puede editar un restaurante.");
			}
			else {
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				u=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!u.getRol().equals("UsuarioRestaurante")) {
					throw new Exception("El identificador "+idUs+" no corresponde a un usuarioRestaurante");
				}
			}

			if(buscarRestaurantePorName(Restaurante.getNombre())==null)
			{
				throw new Exception("El restaurante no existe actualmente.");
			}
			//////transaccion
			this.conn = darConexion();
			daoRestaurantes.setConn(conn);
			daoRestaurantes.updateRestaurante(Restaurante);
			conn.commit();


		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoRestaurantes.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public void deleteRestaurante(String idUs,String nomR) throws Exception {
		DAOTablaRestaurantes daoRestaurantes = new DAOTablaRestaurantes();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{
			//////transaccion

			Usuario u;
			if(idUs.equals("0")) 
			{
				throw new Exception("S�lo un administrador de RotondAndes puede eliminar un restaurante.");
			}
			else {
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				u=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!u.getRol().equals("UsuarioAdmin")) {
					throw new Exception("El identificador "+idUs+" no corresponde a un administrador");
				}
			}

			if(buscarRestaurantePorName(nomR)==null)
			{
				throw new Exception("El restaurante no existe actualmente.");
			}
			this.conn = darConexion();
			daoRestaurantes.setConn(conn);
			daoRestaurantes.deleteRestaurante(nomR);
			conn.commit();


		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoRestaurantes.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}


	public List<Zona> darZonas() throws Exception {
		List<Zona> Zonas;
		DAOTablaZonas daoZonas = new DAOTablaZonas();
		try 
		{			
			this.conn = darConexion();
			daoZonas.setConn(conn);
			Zonas = daoZonas.darZonas();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoZonas.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return Zonas;
	}

	public Zona buscarZonaPorId(Long id) throws Exception {
		Zona Zona=null;
		DAOTablaZonas daoZonas = new DAOTablaZonas();
		try 
		{
			//////transaccion
			this.conn = darConexion();
			daoZonas.setConn(conn);
			Zona = daoZonas.buscarZonaPorId(id);

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoZonas.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return Zona;
	}

	public void addZona(String idUs, Zona Zona) throws Exception {
		DAOTablaZonas daoZonas = new DAOTablaZonas();
		DAOTablaUsuarios daoUs=new DAOTablaUsuarios();
		try 
		{
			Usuario u = null;
			//////transaccion
			this.conn=darConexion();
			daoUs.setConn(conn);
			if(idUs.equals("0")) 
			{
				throw new Exception("S�lo un administrador de RotondANdes puede agregar otra zona");
			}
			else {
				u=daoUs.buscarUsuarioPorId(idUs);			
				if(!u.getRol().equals("UsuarioAdmin")) {
					throw new Exception("El identificador "+idUs+" no corresponde a un administrador");
				}
			}

			this.conn = darConexion();
			daoZonas.setConn(conn);
			daoZonas.addZona(Zona);
			conn.commit();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoZonas.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}
	
	public void addZona(Zona Zona) throws Exception {
		DAOTablaZonas daoZonas = new DAOTablaZonas();
		DAOTablaUsuarios daoUs=new DAOTablaUsuarios();
		try 
		{

			this.conn = darConexion();
			daoZonas.setConn(conn);
			daoZonas.addZona(Zona);
			conn.commit();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoZonas.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public void updateZona(String idUs, Zona Zona) throws Exception {
		DAOTablaZonas daoZonas = new DAOTablaZonas();
		DAOTablaUsuarios daoUs=new DAOTablaUsuarios();
		try 
		{
			Usuario u = null;
			//////transaccion
			this.conn=darConexion();
			daoUs.setConn(conn);
			if(idUs.equals("0")) 
			{
				throw new Exception("S�lo un administrador de RotondANdes puede agregar otra zona");
			}
			else {
				u=daoUs.buscarUsuarioPorId(idUs);			
				if(!u.getRol().equals("UsuarioAdmin")) {
					throw new Exception("El identificador "+idUs+" no corresponde a un administrador");
				}
			}

			if(buscarZonaPorId(Zona.getId())==null)
			{
				throw new Exception("La zona no existe actualmente en el sistema.");
			}
			this.conn = darConexion();
			daoZonas.setConn(conn);
			daoZonas.updateZona(Zona);
			conn.commit();


		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoZonas.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public void deleteZona(String idUs, Zona Zona) throws Exception {
		DAOTablaZonas daoZonas = new DAOTablaZonas();
		DAOTablaUsuarios daoUs=new DAOTablaUsuarios();
		try 
		{
			Usuario u = null;
			//////transaccion
			this.conn=darConexion();
			daoUs.setConn(conn);
			if(idUs.equals("0")) 
			{
				throw new Exception("S�lo un administrador de RotondANdes puede agregar otra zona");
			}
			else {
				u=daoUs.buscarUsuarioPorId(idUs);			
				if(!u.getRol().equals("UsuarioAdmin")) {
					throw new Exception("El identificador "+idUs+" no corresponde a un administrador");
				}
			}
			if(buscarZonaPorId(Zona.getId())==null)
			{
				throw new Exception("La zona no existe actualmente en el sistema.");
			}

			this.conn = darConexion();
			daoZonas.setConn(conn);
			daoZonas.deleteZona(Zona);
			conn.commit();



		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoZonas.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}


	public List<Producto> darProductos() throws Exception {
		List<Producto> Productos;
		DAOTablaProductos daoProductos = new DAOTablaProductos();
		try 
		{			
			this.conn = darConexion();
			daoProductos.setConn(conn);
			Productos = daoProductos.darProductos();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoProductos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return Productos;
	}

	public Producto buscarProductoPorName(String namae) throws Exception {
		Producto Producto=null;
		DAOTablaProductos daoProductos = new DAOTablaProductos();
		try 
		{
			//////transaccion
			this.conn = darConexion();
			daoProductos.setConn(conn);
			Producto = daoProductos.buscarProductoPorName(namae);

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoProductos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return Producto;
	}

	public void addProducto(String idUs,Producto Producto) throws Exception {
		DAOTablaProductos daoProductos = new DAOTablaProductos();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{			
			Usuario u;
			if(idUs.equals("0")) 
			{
				throw new Exception("S�lo un usuarioRestaurante puede ver editar los productos.");
			}
			else {
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				u=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!u.getRol().equals("UsuarioRestaurante")) {
					throw new Exception("El identificador "+idUs+" no corresponde a un usuarioRestaurante");
				}
			}
			//////transaccion
			this.conn = darConexion();
			daoProductos.setConn(conn);
			daoProductos.addProducto(Producto);
			conn.commit();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoProductos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}
	
	public void addProducto(Producto Producto) throws Exception {
		DAOTablaProductos daoProductos = new DAOTablaProductos();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{		
			
			//////transaccion
			this.conn = darConexion();
			daoProductos.setConn(conn);
			daoProductos.addProducto(Producto);
			conn.commit();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoProductos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public void updateProducto(String idUs,Producto Producto) throws Exception {
		DAOTablaProductos daoProductos = new DAOTablaProductos();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{			
			Usuario u;
			if(idUs.equals("0")) 
			{
				throw new Exception("S�lo un usuarioRestaurante puede ver editar los productos.");
			}
			else {
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				u=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!u.getRol().equals("UsuarioRestaurante")) {
					throw new Exception("El identificador "+idUs+" no corresponde a un usuarioRestaurante");
				}
			}

			if(buscarProductoPorName(Producto.getNombre())==null)
			{
				throw new Exception ("El producto no existe actualmente.");

			}
			//////transaccion
			this.conn = darConexion();
			daoProductos.setConn(conn);
			daoProductos.updateProducto(Producto);
			conn.commit();


		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoProductos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public void deleteProducto(String idUs,Producto Producto) throws Exception {
		DAOTablaProductos daoProductos = new DAOTablaProductos();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{			
			Usuario u;
			if(idUs.equals("0")) 
			{
				throw new Exception("S�lo un usuarioRestaurante puede ver editar los productos.");
			}
			else {
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				u=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!u.getRol().equals("UsuarioRestaurante")) {
					throw new Exception("El identificador "+idUs+" no corresponde a un usuarioRestaurante");
				}
			}
			if(buscarProductoPorName(Producto.getNombre())==null)
			{
				throw new Exception ("El producto no existe actualmente.");

			}
			//////transaccion
			this.conn = darConexion();
			daoProductos.setConn(conn);
			daoProductos.deleteProducto(Producto);
			conn.commit();


		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoProductos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}	

	public List<Ingrediente> darIngredientes() throws Exception {
		List<Ingrediente> Ingredientes;
		DAOTablaIngredientes daoIngredientes = new DAOTablaIngredientes();
		try 
		{			
			this.conn = darConexion();
			daoIngredientes.setConn(conn);
			Ingredientes = daoIngredientes.darIngredientes();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoIngredientes.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return Ingredientes;
	}

	public Ingrediente buscarIngredientePorName(String namae) throws Exception {
		Ingrediente Ingrediente=null;
		DAOTablaIngredientes daoIngredientes = new DAOTablaIngredientes();
		try 
		{
			//////transaccion
			this.conn = darConexion();
			daoIngredientes.setConn(conn);
			Ingrediente = daoIngredientes.buscarIngredientePorName(namae);

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoIngredientes.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return Ingrediente;
	}

	public void addIngrediente(String idUs,Ingrediente Ingrediente) throws Exception {
		DAOTablaIngredientes daoIngredientes = new DAOTablaIngredientes();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{			
			Usuario u;
			if(idUs.equals("0")) 
			{
				throw new Exception("S�lo un usuarioRestaurante puede ver editar los ingredientes.");
			}
			else {
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				u=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!u.getRol().equals("UsuarioRestaurante")) {
					throw new Exception("El identificador "+idUs+" no corresponde a un usuarioRestaurante");
				}
			}
			//////transaccion
			this.conn = darConexion();
			daoIngredientes.setConn(conn);
			daoIngredientes.addIngrediente(Ingrediente);
			conn.commit();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoIngredientes.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}
	
	public void addIngrediente(Ingrediente Ingrediente) throws Exception {
		DAOTablaIngredientes daoIngredientes = new DAOTablaIngredientes();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{			
			
			//////transaccion
			this.conn = darConexion();
			daoIngredientes.setConn(conn);
			daoIngredientes.addIngrediente(Ingrediente);
			conn.commit();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoIngredientes.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public void updateIngrediente(String idUs,Ingrediente Ingrediente) throws Exception {
		DAOTablaIngredientes daoIngredientes = new DAOTablaIngredientes();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{			
			Usuario u;
			if(idUs.equals("0")) 
			{
				throw new Exception("S�lo un usuarioRestaurante puede ver editar los ingredientes.");
			}
			else {
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				u=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!u.getRol().equals("UsuarioRestaurante")) {
					throw new Exception("El identificador "+idUs+" no corresponde a un usuarioRestaurante");
				}
			}
			//////transaccion
			if(buscarIngredientePorName(Ingrediente.getNombre())==null)
			{
				throw new Exception("El ingrediente a editar no existe en el sistema actualmente.");

			}
			this.conn = darConexion();
			daoIngredientes.setConn(conn);
			daoIngredientes.updateIngrediente(Ingrediente);
			conn.commit();


		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoIngredientes.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public void deleteIngrediente(String idUs,Ingrediente Ingrediente) throws Exception {
		DAOTablaIngredientes daoIngredientes = new DAOTablaIngredientes();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{			
			Usuario u;
			if(idUs.equals("0")) 
			{
				throw new Exception("S�lo un usuarioRestaurante puede ver editar los ingredientes.");
			}
			else {
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				u=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!u.getRol().equals("UsuarioRestaurante")) {
					throw new Exception("El identificador "+idUs+" no corresponde a un usuarioRestaurante");
				}
			}
			if(buscarIngredientePorName(Ingrediente.getNombre())==null)
			{
				throw new Exception("El ingrediente a editar no existe en el sistema actualmente.");

			}
			//////transaccion
			this.conn = darConexion();
			daoIngredientes.setConn(conn);
			daoIngredientes.deleteIngrediente(Ingrediente);
			conn.commit();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoIngredientes.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}


	public void addRestauranteProducto(String idUs, RestauranteProducto RestauranteProducto) throws Exception {
		DAOTablaRestauranteProducto daoRestauranteProductos = new DAOTablaRestauranteProducto();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{			
			Usuario u;
			if(idUs.equals("0")) 
			{
				throw new Exception("S�lo un usuarioRestaurante puede ver editar los productos que sirve un restaurante");
			}
			else {
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				u=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!u.getRol().equals("UsuarioRestaurante")) {
					throw new Exception("El identificador "+idUs+" no corresponde a un usuarioRestaurante");
				}
			}

			Producto p=buscarProductoPorName(RestauranteProducto.getNomProducto());
			if(p==null)
			{
				throw new Exception("El producto "+RestauranteProducto.getNomProducto()+" no est� registrado en el sistema.");
			}

			if(p.getCategoria().equals("Menu"))
			{
				if(darRestaurantesPorProducto(p.getNombre())!=null)
				{
					if(!darRestaurantesPorProducto(p.getNombre()).isEmpty())
					{
						throw new Exception("Otro restaurante ya sirve un menu con el nombre "+p.getNombre()+". Cambie el nombre del menu.");
					}
				}
			}

			this.conn = darConexion();
			daoRestauranteProductos.setConn(conn);
			daoRestauranteProductos.addRestauranteProducto(RestauranteProducto);
			conn.commit();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoRestauranteProductos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}
	
	

	public List<RestauranteProducto> darRestaurantesPorProducto(String nom) throws Exception {
		List<RestauranteProducto> resPros;
		DAOTablaRestauranteProducto daoResPros = new DAOTablaRestauranteProducto();
		try 
		{			
			this.conn = darConexion();
			daoResPros.setConn(conn);
			resPros = daoResPros.darRestaurantesPorProducto(nom);

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoResPros.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return resPros;
	}

	public RestauranteProducto darRestauranteProducto(String nomR, String nomP) throws Exception {
		RestauranteProducto resPros=null;
		DAOTablaRestauranteProducto daoResPros = new DAOTablaRestauranteProducto();
		try 
		{			
			this.conn = darConexion();
			daoResPros.setConn(conn);
			resPros = daoResPros.darRestauranteProducto(nomR, nomP);

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoResPros.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return resPros;
	}

	public List<RestauranteProducto> darProductosPorRestaurante(String nom) throws Exception {
		List<RestauranteProducto> resPros;
		DAOTablaRestauranteProducto daoResPros = new DAOTablaRestauranteProducto();
		try 
		{			
			this.conn = darConexion();
			daoResPros.setConn(conn);
			resPros = daoResPros.darProductosPorRestaurante(nom);

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoResPros.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return resPros;
	}
	
	public List<RestauranteProducto> darProductosNoMenPorRestaurante(String nom) throws Exception {
		List<RestauranteProducto> resPros;
		DAOTablaRestauranteProducto daoResPros = new DAOTablaRestauranteProducto();
		try 
		{			
			this.conn = darConexion();
			daoResPros.setConn(conn);
			resPros = daoResPros.darProductosNoMenPorRestaurante(nom);

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoResPros.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return resPros;
	}

	public void deleteRestauranteProducto(String idUs,RestauranteProducto RestauranteProducto) throws Exception {
		DAOTablaRestauranteProducto daoRestauranteProductos = new DAOTablaRestauranteProducto();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{			
			Usuario u;
			if(idUs.equals("0")) 
			{
				throw new Exception("S�lo un usuarioRestaurante puede ver editar los productos que sirve un restaurante");
			}
			else {
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				u=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!u.getRol().equals("UsuarioRestaurante")) {
					throw new Exception("El identificador "+idUs+" no corresponde a un usuarioRestaurante");
				}
			}

			this.conn = darConexion();
			daoRestauranteProductos.setConn(conn);
			daoRestauranteProductos.deleteRestauranteProducto(RestauranteProducto);
			conn.commit();


		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoRestauranteProductos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}	

	public List<Pedido> darPedidos() throws Exception {
		List<Pedido> Pedidos;
		DAOTablaPedidos daoPedidos = new DAOTablaPedidos();
		try 
		{			
			this.conn = darConexion();
			daoPedidos.setConn(conn);
			Pedidos = daoPedidos.darPedidos();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoPedidos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return Pedidos;
	}

	public Pedido buscarPedidoPorId(Long id) throws Exception {
		Pedido Pedido=null;
		DAOTablaPedidos daoPedidos = new DAOTablaPedidos();
		DAOTablaEquivalenciaProductoPedido daoEqPros = new DAOTablaEquivalenciaProductoPedido();
		DAOTablaEquivalenciaIngredientePedido daoEqIngs = new DAOTablaEquivalenciaIngredientePedido();	
		try 
		{
			//////transaccion
			this.conn=darConexion();
			daoEqPros.setConn(conn);
			List <EquivalenciaProductoPedido> listaEqProdActual=daoEqPros.darEquivalenciasProductoPorPedido(id);
			ArrayList<EquivalenciaProducto> listaUsP=new ArrayList<EquivalenciaProducto>();

			this.conn=darConexion();
			daoEqIngs.setConn(conn);
			List<EquivalenciaIngredientePedido> listaEqIngActual=daoEqIngs.darEquivalenciasIngredientePorPedido(id);
			ArrayList<EquivalenciaIngrediente> listaUsI=new ArrayList<EquivalenciaIngrediente>();

			this.conn = darConexion();
			daoPedidos.setConn(conn);
			Pedido = daoPedidos.darPedidoPorId(id);

			for(EquivalenciaIngredientePedido h:listaEqIngActual)
			{
				listaUsI.add(new EquivalenciaIngrediente(h.getNomEquivalencia(), h.getNomIngrediente(), h.getNomRestaurante()));
			}

			for(EquivalenciaProductoPedido h:listaEqProdActual)
			{
				listaUsP.add(new EquivalenciaProducto(h.getNomEquivalencia(), h.getNomProducto(), h.getNomRestaurante()));
			}


			Pedido.setEquivalenciasIngredientes(listaUsI);
			Pedido.setEquivalenciasProductos(listaUsP);

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoPedidos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return Pedido;
	}

	public void addPedido(String idUs, List<Pedido> Pedidos) throws Exception {

		DAOTablaPedidos daoPedidos = new DAOTablaPedidos();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{			
			Usuario x;
			if(!idUs.equals("0")) 
			{
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				x=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!x.getRol().equals("UsuarioCliente")) 
				{
					throw new Exception("El identificador "+idUs+" no corresponde a un usuarioCliente");
				}
			}

			//////transaccion

			for (int i = 0; i <Pedidos.size(); i++) {

				if(Pedidos.get(i).getIdUsuario()!=null)
				{
					this.conn=darConexion();
					daoUsuarios.setConn(conn);
					if(!idUs.equals(Pedidos.get(i).getIdUsuario()))
					{
						throw new Exception("El usuario con identificacion "+idUs+" no puede ordenar un pedido para "+Pedidos.get(i).getIdUsuario());
					}

					Usuario u=daoUsuarios.buscarUsuarioPorId(Pedidos.get(i).getIdUsuario());
					if(!u.getRol().equals("UsuarioCliente"))
						throw new Exception("El identificador "+Pedidos.get(i).getIdUsuario()+" no corresponde a un cliente");
				}

				if (darMesaPorId(Pedidos.get(i).getNumMesa()) == null) {

					throw new Exception("La mesa asignada al pedido con id" + Pedidos.get(i).getIdPedido()+ " no existe");
				}

			} 
			this.conn = darConexion();
			daoPedidos.setConn(conn);
			daoPedidos.RF15((ArrayList)Pedidos);
			conn.commit();
		}
		catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage()+"Error prueba 1");
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage()+ "Error prueba2");
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoPedidos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public void addPedido(String idUs, Pedido Pedido) throws Exception {
		DAOTablaPedidos daoPedidos = new DAOTablaPedidos();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{			
			Usuario x;
			if(!idUs.equals("0")) 
			{
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				x=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!x.getRol().equals("UsuarioCliente")) {
					throw new Exception("El identificador "+idUs+" no corresponde a un usuarioCliente");
				}
			}

			//////transaccion
			if(Pedido.getIdUsuario()!=null)
			{
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				if(!idUs.equals(Pedido.getIdUsuario()))
				{
					throw new Exception("El usuario con identificacion "+idUs+" no puede ordenar un pedido para "+Pedido.getIdUsuario());
				}

				Usuario u=daoUsuarios.buscarUsuarioPorId(Pedido.getIdUsuario());
				if(!u.getRol().equals("UsuarioCliente"))
					throw new Exception("El identificador "+Pedido.getIdUsuario()+" no corresponde a un cliente");
			}

			this.conn = darConexion();
			daoPedidos.setConn(conn);
			daoPedidos.addPedidoInd(Pedido);
			conn.commit();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoPedidos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public void updatePedido(String idUs,Pedido Pedido) throws Exception {
		DAOTablaPedidos daoPedidos = new DAOTablaPedidos();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{			
			Usuario x;
			if(!idUs.equals("0")) 
			{
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				x=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!x.getRol().equals("UsuarioCliente")) {
					throw new Exception("El identificador "+idUs+" no corresponde a un usuarioCliente");
				}
			}	
			if(buscarPedidoPorId(Pedido.getIdPedido())==null)
			{
				throw new Exception ("El pedido no existe actualmente.");

			}

			if(Pedido.getIdUsuario()!=null)
			{
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				if(!idUs.equals(Pedido.getIdUsuario()))
				{
					throw new Exception("El usuario con identificacion "+idUs+" no puede editar un pedido de "+Pedido.getIdUsuario());
				}
			}
			//////transaccion
			this.conn = darConexion();
			daoPedidos.setConn(conn);
			daoPedidos.updatePedido(Pedido);
			conn.commit();


		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoPedidos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}
	
	public void updatePedidoDatos(Pedido Pedido) throws Exception {
		DAOTablaPedidos daoPedidos = new DAOTablaPedidos();
		try 
		{	
			//////transaccion
			this.conn = darConexion();
			daoPedidos.setConn(conn);
			daoPedidos.updatePedido(Pedido);
			conn.commit();


		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoPedidos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}



	public void deletePedido(String idUs,Pedido Pedido) throws Exception {
		DAOTablaPedidos daoPedidos = new DAOTablaPedidos();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{			
			Usuario x;
			if(!idUs.equals("0")) 
			{
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				x=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!x.getRol().equals("UsuarioCliente")) {
					throw new Exception("El identificador "+idUs+" no corresponde a un usuarioCliente");
				}
			}			

			if(Pedido.getIdUsuario()!=null)
			{
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				if(!idUs.equals(Pedido.getIdUsuario()))
				{
					throw new Exception("El usuario con identificacion "+idUs+" no puede editar un pedido de "+Pedido.getIdUsuario());
				}}
			if(buscarPedidoPorId(Pedido.getIdPedido())==null)
			{
				throw new Exception ("El pedido no existe actualmente.");

			}
			//////transaccion
			this.conn = darConexion();
			daoPedidos.setConn(conn);
			daoPedidos.deletePedido(Pedido);
			conn.commit();


		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoPedidos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public List<ProductoPedido> darProductoPedidosPorPedido(Long idPedido) throws Exception {
		List<ProductoPedido> prodPedidos;
		DAOTablaProductoPedido daoProductoPedidos = new DAOTablaProductoPedido();
		try 
		{			
			this.conn = darConexion();
			daoProductoPedidos.setConn(conn);
			prodPedidos = daoProductoPedidos.darRestauranteProductosPorIdPedido(idPedido);

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoProductoPedidos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return prodPedidos;
	}



	public void addProductoPedido(String idUs, ProductoPedido prodPed) throws Exception {
		DAOTablaProductoPedido daoProductoPedidos = new DAOTablaProductoPedido();
		DAOTablaPedidos daoPeds= new DAOTablaPedidos();
		DAOTablaRestauranteProducto daoResPros= new DAOTablaRestauranteProducto();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();

		try 
		{			
			Usuario x;
			if(!idUs.equals("0")) 
			{
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				x=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!x.getRol().equals("UsuarioCliente")) {
					throw new Exception("El identificador "+idUs+" no corresponde a un usuarioCliente");
				}
			}
			//////transaccion
			this.conn = darConexion();
			daoResPros.setConn(conn);
			RestauranteProducto testeo=darRestauranteProducto(prodPed.getNomRestaurante(), prodPed.getNomProducto());
			if(testeo==null)
				throw new Exception("El producto "+prodPed.getNomProducto()+" no se vende en "+prodPed.getNomRestaurante());
			else if(testeo.getUnidadesDisponibles()<=0)
				throw new Exception("El producto "+prodPed.getNomProducto()+" se acab� en "+prodPed.getNomRestaurante());
			else
			{
				Producto pact=buscarProductoPorName(prodPed.getNomProducto());
				if(pact.getCategoria().equals("Menu"))
				{					
					for(ProductoMenu vaina:darProductosPorMenu(pact.getNombre()))
					{						
						String nomP=vaina.getNomProducto();
						String nomR=prodPed.getNomRestaurante();
						Long idz=prodPed.getIdPedido(); 
						addProductoPedido(idUs,new ProductoPedido(idz, nomP, nomR));
					}
				}				

				this.conn = darConexion();
				daoResPros.setConn(conn);

				int n=testeo.getUnidadesDisponibles();
				testeo.setUnidadesDisponibles(n-1);
				daoResPros.updateRestauranteProducto(testeo);
			}	

			this.conn=darConexion();
			daoPeds.setConn(conn);
			Pedido pi=daoPeds.darPedidoPorId(prodPed.getIdPedido());
			double c=testeo.getPrecio();
			double p=pi.getPrecioTotal();
			pi.setPrecioTotal(p+c);
			updatePedido(idUs,pi);

			this.conn = darConexion();
			daoProductoPedidos.setConn(conn);
			daoProductoPedidos.addProductoPedido(prodPed);
			conn.commit();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoProductoPedidos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}
	
	public void addProductoPedidoDatos(ProductoPedido prodPed) throws Exception {
		DAOTablaProductoPedido daoProductoPedidos = new DAOTablaProductoPedido();
		DAOTablaPedidos daoPeds= new DAOTablaPedidos();
		DAOTablaRestauranteProducto daoResPros= new DAOTablaRestauranteProducto();

		try 
		{	
			//////transaccion
			this.conn = darConexion();
			daoResPros.setConn(conn);
			RestauranteProducto testeo=darRestauranteProducto(prodPed.getNomRestaurante(), prodPed.getNomProducto());
			if(testeo==null)
				return;
			else if(testeo.getUnidadesDisponibles()<=0)
				return;
			else
			{
				Producto pact=buscarProductoPorName(prodPed.getNomProducto());
				if(pact.getCategoria().equals("Menu"))
				{					
					for(ProductoMenu vaina:darProductosPorMenu(pact.getNombre()))
					{						
						String nomP=vaina.getNomProducto();
						String nomR=prodPed.getNomRestaurante();
						Long idz=prodPed.getIdPedido(); 
						addProductoPedidoDatos(new ProductoPedido(idz, nomP, nomR));
					}
				}				

				this.conn = darConexion();
				daoResPros.setConn(conn);

				int n=testeo.getUnidadesDisponibles();
				testeo.setUnidadesDisponibles(n-1);
				daoResPros.updateRestauranteProducto(testeo);
			}	

			this.conn=darConexion();
			daoPeds.setConn(conn);
			Pedido pi=daoPeds.darPedidoPorId(prodPed.getIdPedido());
			double c=testeo.getPrecio();
			double p=pi.getPrecioTotal();
			pi.setPrecioTotal(p+c);
			updatePedidoDatos(pi);

			this.conn = darConexion();
			daoProductoPedidos.setConn(conn);
			daoProductoPedidos.addProductoPedido(prodPed);
			conn.commit();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoProductoPedidos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}
	
	public void llenarProductoPedido() throws Exception
	{
		for(int i=20000;i<30000;i++)
		{
			Long idz=(long)i;
			Random randz=new Random();
			for(int j=1;j<7;j++)
			{
				int res=randz.nextInt(1000000)+1;
				String nomR="restaurante"+res;
				List<RestauranteProducto> prods=darProductosPorRestaurante(nomR);
				if(!prods.isEmpty()) {
				RestauranteProducto random = prods.get(randz.nextInt(prods.size()));
				ProductoPedido n=new ProductoPedido(idz,random.getNomProducto(),random.getNomRestaurante());
				addProductoPedidoDatos(n);}
			}			
		}
	}



	public void deleteProductoPedido(String idUs,ProductoPedido ProductoPedido) throws Exception {
		DAOTablaProductoPedido daoProductoPedidos = new DAOTablaProductoPedido();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{			
			Usuario x;
			if(!idUs.equals("0")) 
			{
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				x=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!x.getRol().equals("UsuarioCliente")) {
					throw new Exception("El identificador "+idUs+" no corresponde a un usuarioCliente");
				}
			}
			//////transaccion
			this.conn = darConexion();
			daoProductoPedidos.setConn(conn);
			daoProductoPedidos.deleteProductoPedido(ProductoPedido);
			conn.commit();


		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoProductoPedidos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}


	public void agregarPreferencia(String idUs,Preferencia preferencia) throws Exception {

		DAOTablaPreferencias daoPreferencias = new DAOTablaPreferencias();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{			
			Usuario x;
			if(idUs.equals("0")) 
			{
				throw new Exception("S�lo un usuarioCliente puede ver editar las preferencias.");
			}
			else {
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				x=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!x.getRol().equals("UsuarioCliente")||!x.getIdentificacion().equals(preferencia.getIdCliente())) {
					throw new Exception("S�lo el usuario con id "+preferencia.getIdCliente()+" puede editar preferencias de este usuario");
				}
			}
			//////transaccion
			this.conn=darConexion();
			daoUsuarios.setConn(conn);
			Usuario u=daoUsuarios.buscarUsuarioPorId(preferencia.getIdCliente());
			if(!u.getRol().equals("UsuarioCliente"))
				throw new Exception("El identificador "+preferencia.getIdCliente()+" no corresponde a un cliente");

			this.conn = darConexion();
			daoPreferencias.setConn(conn);
			daoPreferencias.addPreferenciaUsuario(preferencia);
			conn.commit();		
		}
		catch( SQLException e )
		{
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoPreferencias.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			}
			catch(SQLException e) {
				System.err.println("SQLException closing resources:" + e.getMessage());
				e.printStackTrace();
				throw e;
			}

		}

	}

	public void updatePreferencia (String idUs, Preferencia pref) throws SQLException, Exception {

		DAOTablaPreferencias preferencias = new DAOTablaPreferencias();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{			
			Usuario x;
			if(idUs.equals("0")) 
			{
				throw new Exception("S�lo un usuarioCliente puede ver editar las preferencias.");
			}
			else {
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				x=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!x.getRol().equals("UsuarioCliente")||!x.getIdentificacion().equals(pref.getIdCliente())) {
					throw new Exception("S�lo el usuario con id "+pref.getIdCliente()+" puede editar preferencias de este usuario");
				}
			}

			//////transaccion
			this.conn = darConexion();
			preferencias.setConn(conn);
			preferencias.updatePreferencia(pref);
			conn.commit();


		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				preferencias.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public List <Preferencia> buscarPreferenciasPorIdCliente (String idUs,String idCliente) throws Exception, SQLException {

		List<Preferencia> preferencias;
		DAOTablaPreferencias daoPreferencias = new DAOTablaPreferencias();
		DAOTablaUsuarios daoUsuarios=new DAOTablaUsuarios();
		try 
		{			
			Usuario x;
			if(idUs.equals("0")) 
			{
				throw new Exception("S�lo un usuarioCliente puede ver editar las preferencias.");
			}
			else {
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				x=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!x.getRol().equals("UsuarioCliente")||!x.getIdentificacion().equals(idCliente)) {
					throw new Exception("S�lo el usuario con id "+idCliente+" puede editar preferencias de este usuario");
				}
			}		
			this.conn=darConexion();
			daoUsuarios.setConn(conn);
			Usuario u=daoUsuarios.buscarUsuarioPorId(idCliente);
			if(!u.getRol().equals("UsuarioCliente"))
				throw new Exception("El identificador "+idCliente+" no corresponde a un cliente");

			this.conn = darConexion();
			daoPreferencias.setConn(conn);
			preferencias = daoPreferencias.darPreferencias(idCliente);

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoPreferencias.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return preferencias;

	}


	public void deletePreferencia(String idUs,Preferencia preferencia) throws Exception, SQLException {
		DAOTablaPreferencias daoPreferencias = new DAOTablaPreferencias();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{			
			Usuario x;
			if(idUs.equals("0")) 
			{
				throw new Exception("S�lo un usuarioCliente puede ver editar las preferencias.");
			}
			else {
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				x=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!x.getRol().equals("UsuarioCliente")||!x.getIdentificacion().equals(preferencia.getIdCliente())) {
					throw new Exception("S�lo el usuario con id "+preferencia.getIdCliente()+" puede editar preferencias de este usuario");
				}
			}
			//////transaccion
			this.conn = darConexion();
			daoPreferencias.setConn(conn);
			daoPreferencias.deletePreferencia(preferencia);
			conn.commit();


		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoPreferencias.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public List<ProductoMenu> darProductosPorMenu(String nomM) throws Exception {
		List<ProductoMenu> prodPedidos;
		DAOTablaProductoMenu daoProductoMenus = new DAOTablaProductoMenu();
		try 
		{			
			this.conn = darConexion();
			daoProductoMenus.setConn(conn);
			prodPedidos = daoProductoMenus.darProductosPorMenu(nomM);

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoProductoMenus.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return prodPedidos;
	}



	public void addProductoMenu(String idUs,ProductoMenu prodPed) throws Exception 
	{
		DAOTablaProductoMenu daoProductoMenus = new DAOTablaProductoMenu();
		DAOTablaProductos daoProds= new DAOTablaProductos();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{			
			Usuario u;
			if(idUs.equals("0")) 
			{
				throw new Exception("S�lo un usuarioRestaurante puede ver editar los menus.");
			}
			else {
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				u=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!u.getRol().equals("UsuarioRestaurante")) {
					throw new Exception("El identificador "+idUs+" no corresponde a un usuarioRestaurante");
				}
			}
			//////transaccion
			this.conn = darConexion();
			daoProds.setConn(conn);
			Producto menuActual=daoProds.buscarProductoPorName(prodPed.getNomMenu());
			if(menuActual==null)
			{
				throw new Exception("El men� "+prodPed.getNomMenu()+" no existe.");
			}
			String nomR="";
			for(RestauranteProducto vaina:darRestaurantesPorProducto(menuActual.getNombre()))
			{
				nomR=vaina.getNomRestaurante();
			}
			if(nomR.equals(""))
			{
				throw new Exception("El men� "+prodPed.getNomMenu()+" no se vende en ning�n restaurante, para agregar productos a un men�"
						+ " este debe ser vendido en el mismo restaurante que sus productos.");
			}
			if(darRestauranteProducto(nomR, prodPed.getNomProducto())==null)
			{
				throw new Exception("El producto "+prodPed.getNomProducto()+" no se vende en "+nomR+". Para agregarlo al menu "+prodPed.getNomMenu()+
						", el producto debe venderse en el mismo restaurante.");
			}
			List<ProductoMenu> listica=darProductosPorMenu(prodPed.getNomMenu());
			Producto base=daoProds.buscarProductoPorName(prodPed.getNomProducto());
			for(ProductoMenu p:listica)
			{
				Producto actual=daoProds.buscarProductoPorName(p.getNomProducto());				
				if(base.getCategoria().equals(actual.getCategoria()))
					throw new Exception("El menu no puede tener mas de un producto de la categoria "+base.getCategoria());
			}

			this.conn = darConexion();
			daoProductoMenus.setConn(conn);
			daoProductoMenus.addProductoMenu(prodPed);
			conn.commit();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoProductoMenus.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}
	
	public void addProductoMenuDatos(ProductoMenu prodPed) throws Exception 
	{
		DAOTablaProductoMenu daoProductoMenus = new DAOTablaProductoMenu();
		DAOTablaProductos daoProds= new DAOTablaProductos();
		try 
		{			
			
			this.conn = darConexion();
			daoProds.setConn(conn);
			Producto menuActual=daoProds.buscarProductoPorName(prodPed.getNomMenu());
			if(menuActual==null)
			{
				return;
			}
			String nomR="";
			for(RestauranteProducto vaina:darRestaurantesPorProducto(menuActual.getNombre()))
			{
				nomR=vaina.getNomRestaurante();
			}
			if(nomR.equals(""))
			{
				return;

			}
			if(darRestauranteProducto(nomR, prodPed.getNomProducto())==null)
			{
				return;

			}
			List<ProductoMenu> listica=darProductosPorMenu(prodPed.getNomMenu());
			Producto base=daoProds.buscarProductoPorName(prodPed.getNomProducto());
			for(ProductoMenu p:listica)
			{
				Producto actual=daoProds.buscarProductoPorName(p.getNomProducto());				
				if(base.getCategoria().equals(actual.getCategoria()))
					return;

			}

			this.conn = darConexion();
			daoProductoMenus.setConn(conn);
			daoProductoMenus.addProductoMenu(prodPed);
			conn.commit();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoProductoMenus.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public void llenarMenus() throws Exception
	{
		DAOTablaRestauranteProducto daoResProds= new DAOTablaRestauranteProducto();
		try 
		{		
			Random randomizer = new Random();
			for(int i=90000;i<1000000;i++)
			{
				this.conn = darConexion();
				daoResProds.setConn(conn);
				String nom="restaurante"+i;
				ArrayList<Producto> menus=daoResProds.darMenusPorRestaurante(nom);
				List<RestauranteProducto> prods=darProductosNoMenPorRestaurante(nom);
				for(Producto mAct:menus)
				{
					for(int j=1;j<6;j++)
					{
						RestauranteProducto random = prods.get(randomizer.nextInt(prods.size()));
						ProductoMenu n=new ProductoMenu(mAct.getNombre(), random.getNomProducto());
						addProductoMenuDatos(n);
					}

				}

			}



		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoResProds.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	
	}




	public void deleteProductoMenu(String idUs,ProductoMenu ProductoMenu) throws Exception {
		DAOTablaProductoMenu daoProductoMenus = new DAOTablaProductoMenu();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{			
			Usuario u;
			if(idUs.equals("0")) 
			{
				throw new Exception("S�lo un usuarioRestaurante puede ver editar los menus.");
			}
			else {
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				u=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!u.getRol().equals("UsuarioRestaurante")) {
					throw new Exception("El identificador "+idUs+" no corresponde a un usuarioRestaurante");
				}
			}
			//////transaccion
			this.conn = darConexion();
			daoProductoMenus.setConn(conn);
			daoProductoMenus.deleteProductoMenu(ProductoMenu);
			conn.commit();


		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoProductoMenus.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public void addProductoIngrediente(String idUs,ProductoIngrediente ProductoIngrediente) throws Exception {
		DAOTablaProductoIngrediente daoProductoIngredientes = new DAOTablaProductoIngrediente();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{			
			Usuario u;
			if(idUs.equals("0")) 
			{
				throw new Exception("S�lo un usuarioRestaurante puede ver editar los productos que sirve un restaurante");
			}
			else {
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				u=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!u.getRol().equals("UsuarioRestaurante")) {
					throw new Exception("El identificador "+idUs+" no corresponde a un usuarioRestaurante");
				}
			}
			//////transaccion
			this.conn = darConexion();
			daoProductoIngredientes.setConn(conn);
			daoProductoIngredientes.addProductoIngrediente(ProductoIngrediente);
			conn.commit();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoProductoIngredientes.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public List<ProductoIngrediente> darIngredientesPorProducto(String nomP) throws Exception {
		List<ProductoIngrediente> resPros;
		DAOTablaProductoIngrediente daoResPros = new DAOTablaProductoIngrediente();
		try 
		{			
			this.conn = darConexion();
			daoResPros.setConn(conn);
			resPros = daoResPros.darIngredientesPorProducto(nomP);

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoResPros.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return resPros;
	}



	public void deleteProductoIngrediente(String idUs,ProductoIngrediente ProductoIngrediente) throws Exception {
		DAOTablaProductoIngrediente daoProductoIngredientes = new DAOTablaProductoIngrediente();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{			
			Usuario u;
			if(idUs.equals("0")) 
			{
				throw new Exception("S�lo un usuarioRestaurante puede ver editar los ingredientes de un producto");
			}
			else {
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				u=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!u.getRol().equals("UsuarioRestaurante")) {
					throw new Exception("El identificador "+idUs+" no corresponde a un usuarioRestaurante");
				}
			}
			//////transaccion
			this.conn = darConexion();
			daoProductoIngredientes.setConn(conn);
			daoProductoIngredientes.deleteProductoIngrediente(ProductoIngrediente);
			conn.commit();


		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoProductoIngredientes.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public List<EquivalenciaProducto> darEquivalenciasProducto() throws SQLException, Exception
	{
		List<EquivalenciaProducto> eqPros;
		DAOTablaEquivalenciaProducto daoEqPros = new DAOTablaEquivalenciaProducto();
		try 
		{			
			this.conn = darConexion();
			daoEqPros.setConn(conn);
			eqPros = daoEqPros.darEquivalenciasProductos();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoEqPros.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return eqPros;
	}

	public List<EquivalenciaProducto> darEquivalenciasProductoPorProductoEnRestaurante(String nomP, String nomR) throws SQLException, Exception
	{
		List<EquivalenciaProducto> eqPros;
		DAOTablaEquivalenciaProducto daoEqPros = new DAOTablaEquivalenciaProducto();
		try 
		{			
			this.conn = darConexion();
			daoEqPros.setConn(conn);
			eqPros = daoEqPros.darEquivalenciasPorProductoEnRestaurante(nomP, nomR);

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoEqPros.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return eqPros;
	}

	public List<EquivalenciaProducto> darEquivalenciasProductoPorEqEnRestaurante(String nomP, String nomR) throws SQLException, Exception
	{
		List<EquivalenciaProducto> eqPros=null;
		DAOTablaEquivalenciaProducto daoEqPros = new DAOTablaEquivalenciaProducto();
		try 
		{			
			this.conn = darConexion();
			daoEqPros.setConn(conn);
			eqPros = daoEqPros.darEquivalenciasPorNomEquivalenciaEnRestaurante(nomP, nomR);

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoEqPros.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return eqPros;
	}



	public void addEquivalenciaProducto(String idUs, EquivalenciaProducto eqPro) throws Exception {
		DAOTablaEquivalenciaProducto daoEquivalenciaProductos = new DAOTablaEquivalenciaProducto();		
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		DAOTablaProductos daoProds=new DAOTablaProductos();
		try 
		{			
			Usuario u;
			if(idUs.equals("0")) 
			{
				throw new Exception("S�lo un usuarioRestaurante puede editar las equivalencias de un Producto");
			}
			else {
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				u=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!u.getRol().equals("UsuarioRestaurante")) {
					throw new Exception("El identificador "+idUs+" no corresponde a un usuarioRestaurante");
				}
			}
			//////transaccion
			List<EquivalenciaProducto> listaChequeo=darEquivalenciasProductoPorEqEnRestaurante(eqPro.getnomEquivalencia(), eqPro.getNomRestaurante());
			if(listaChequeo!=null)
			{
				if(!listaChequeo.isEmpty())
					for(EquivalenciaProducto h:listaChequeo)
					{
						Producto p1=buscarProductoPorName(h.getNomProducto());
						Producto p2=buscarProductoPorName(eqPro.getNomProducto());
						if(!p1.getCategoria().equals(p2.getCategoria()))
						{
							throw new Exception ("Dos productos equivalentes deben ser de una misma categoria.");
						}
					}
			}		
			this.conn = darConexion();
			daoEquivalenciaProductos.setConn(conn);
			daoEquivalenciaProductos.addEquivalenciaProducto(eqPro);
			conn.commit();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoEquivalenciaProductos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public void deleteEquivalenciaProducto(String idUs,EquivalenciaProducto EquivalenciaProducto) throws Exception {
		DAOTablaEquivalenciaProducto daoEquivalenciaProductos = new DAOTablaEquivalenciaProducto();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{			
			Usuario u;
			if(idUs.equals("0")) 
			{
				throw new Exception("S�lo un usuarioRestaurante puede editar las equivalencias de un producto");
			}
			else {
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				u=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!u.getRol().equals("UsuarioRestaurante")) {
					throw new Exception("El identificador "+idUs+" no corresponde a un usuarioRestaurante");
				}
			}
			//////transaccion
			this.conn = darConexion();
			daoEquivalenciaProductos.setConn(conn);
			daoEquivalenciaProductos.deleteEquivalenciaProducto(EquivalenciaProducto);
			conn.commit();


		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoEquivalenciaProductos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public List<EquivalenciaIngrediente> darEquivalenciasIngrediente() throws SQLException, Exception
	{
		List<EquivalenciaIngrediente> eqPros;
		DAOTablaEquivalenciaIngrediente daoEqPros = new DAOTablaEquivalenciaIngrediente();
		try 
		{			
			this.conn = darConexion();
			daoEqPros.setConn(conn);
			eqPros = daoEqPros.darEquivalenciasIngredientes();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoEqPros.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return eqPros;
	}

	public List<EquivalenciaIngrediente> darEquivalenciasIngredientePorIngredienteEnRestaurante(String nomP, String nomR) throws SQLException, Exception
	{
		List<EquivalenciaIngrediente> eqPros;
		DAOTablaEquivalenciaIngrediente daoEqPros = new DAOTablaEquivalenciaIngrediente();
		try 
		{			
			this.conn = darConexion();
			daoEqPros.setConn(conn);
			eqPros = daoEqPros.darEquivalenciasPorIngredienteEnRestaurante(nomP, nomR);

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoEqPros.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return eqPros;
	}



	public void addEquivalenciaIngrediente(String idUs, EquivalenciaIngrediente eqPro) throws Exception {
		DAOTablaEquivalenciaIngrediente daoEquivalenciaIngredientes = new DAOTablaEquivalenciaIngrediente();		
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{			
			Usuario u;
			if(idUs.equals("0")) 
			{
				throw new Exception("S�lo un usuarioRestaurante puede editar las equivalencias de un Ingrediente");
			}
			else {
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				u=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!u.getRol().equals("UsuarioRestaurante")) {
					throw new Exception("El identificador "+idUs+" no corresponde a un usuarioRestaurante");
				}
			}
			//////transaccion
			this.conn = darConexion();
			daoEquivalenciaIngredientes.setConn(conn);
			daoEquivalenciaIngredientes.addEquivalenciaIngrediente(eqPro);
			conn.commit();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoEquivalenciaIngredientes.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public void deleteEquivalenciaIngrediente(String idUs,EquivalenciaIngrediente eqIng) throws Exception {
		DAOTablaEquivalenciaIngrediente daoEquivalenciaIngredientes = new DAOTablaEquivalenciaIngrediente();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{			
			Usuario u;
			if(idUs.equals("0")) 
			{
				throw new Exception("S�lo un usuarioRestaurante puede editar las equivalencias de un Ingrediente");
			}
			else {
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				u=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!u.getRol().equals("UsuarioRestaurante")) {
					throw new Exception("El identificador "+idUs+" no corresponde a un usuarioRestaurante");
				}
			}
			//////transaccion
			this.conn = darConexion();
			daoEquivalenciaIngredientes.setConn(conn);
			daoEquivalenciaIngredientes.deleteEquivalenciaIngrediente(eqIng);
			conn.commit();


		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoEquivalenciaIngredientes.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}


	public void addEquivalenciaProductoPedido(String idUs, EquivalenciaProductoPedido eqPro) throws Exception {

		DAOTablaEquivalenciaProductoPedido daoEquivalenciaProductos = new DAOTablaEquivalenciaProductoPedido();		
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		DAOTablaProductoPedido daoProductoPedidos = new DAOTablaProductoPedido();
		DAOTablaEquivalenciaProducto daoEqPros = new DAOTablaEquivalenciaProducto();
		DAOTablaPedidos daoPedidos = new DAOTablaPedidos();
		DAOTablaRestauranteProducto daoResPros= new DAOTablaRestauranteProducto();

		try 
		{			
			this.conn=darConexion();
			daoPedidos.setConn(conn);
			Pedido pew=daoPedidos.darPedidoPorId(eqPro.getIdPedido());
			if(pew.getIdUsuario()!=null)
			{
				Usuario x;
				if(!idUs.equals("0")) 
				{
					this.conn=darConexion();
					daoUsuarios.setConn(conn);
					x=daoUsuarios.buscarUsuarioPorId(idUs);			
					if(!x.getRol().equals("UsuarioCliente")) {
						throw new Exception("El identificador "+idUs+" no corresponde a un usuarioCliente");
					}
					if(!pew.getIdUsuario().equals(idUs))
					{
						throw new Exception ("El pedido con id"+eqPro.getIdPedido()+" s�lo puede ser editado por el usuario con id "+pew.getIdUsuario());

					}
				}
				else
				{
					throw new Exception ("El pedido con id"+eqPro.getIdPedido()+" s�lo puede ser editado por el usuario con id "+pew.getIdUsuario());
				}
			}
			//////transaccion
			this.conn=darConexion();
			daoProductoPedidos.setConn(conn);
			List<ProductoPedido> listaProductosActuales=daoProductoPedidos.darRestauranteProductosPorIdPedido(eqPro.getIdPedido());
			if(listaProductosActuales==null||listaProductosActuales.isEmpty())
			{
				throw new Exception("No hay productos en el pedido con id "+eqPro.getIdPedido());				
			}
			boolean varz=false;
			for(ProductoPedido z:listaProductosActuales)
			{
				List<EquivalenciaProducto> lact=darEquivalenciasProductoPorProductoEnRestaurante(z.getNomProducto(), z.getNomRestaurante());
				for(EquivalenciaProducto iact:lact)
				{
					if(iact.getnomEquivalencia().equals(eqPro.getNomEquivalencia()))
					{
						varz=true;


						this.conn = darConexion();
						daoResPros.setConn(conn);
						RestauranteProducto testeo=daoResPros.darRestauranteProducto(z.getNomRestaurante(), z.getNomProducto());
						int n=testeo.getUnidadesDisponibles();
						testeo.setUnidadesDisponibles(n+1);
						daoResPros.updateRestauranteProducto(testeo);

						this.conn = darConexion();
						daoResPros.setConn(conn);
						RestauranteProducto testeo2=daoResPros.darRestauranteProducto(eqPro.getNomRestaurante(), eqPro.getNomProducto());
						int n2=testeo2.getUnidadesDisponibles();
						testeo2.setUnidadesDisponibles(n2-1);
						daoResPros.updateRestauranteProducto(testeo2);
					}

				}
			}

			if(!varz)
			{
				throw new Exception("La equivalencia "+eqPro.getNomEquivalencia()+" no puede ser aplicada en el pedido, ya que no tiene un producto equivalente. Para hacer intercambiar unproducto por uno equivalente, el producto que desea cambiar debe estar en el pedido");
			}

			this.conn=darConexion();
			daoEqPros.setConn(conn);
			List<EquivalenciaProducto> listaEq=daoEqPros.darEquivalenciasPorProductoEnRestaurante(eqPro.getNomProducto(), eqPro.getNomRestaurante());
			if(listaEq==null||listaEq.isEmpty())
			{
				throw new Exception ("El producto "+eqPro.getNomProducto()+" no es equivalente a ning�n otro en el restaurante "+eqPro.getNomRestaurante());
			}
			boolean varz2=false;
			for(EquivalenciaProducto q:listaEq)
			{
				if(q.getnomEquivalencia().equals(eqPro.getNomEquivalencia())&&q.getNomProducto().equals(eqPro.getNomProducto())&&q.getNomRestaurante().equals(eqPro.getNomRestaurante()))
					varz2=true;
			}
			if(!varz2)
			{
				throw new Exception ("La equivalencia "+eqPro.getNomEquivalencia()+" no puede ser aplicada para "+eqPro.getNomProducto()+" en "+eqPro.getNomRestaurante());
			}				


			this.conn = darConexion();
			daoEquivalenciaProductos.setConn(conn);
			daoEquivalenciaProductos.addEquivalenciaProductoPedido(eqPro);
			conn.commit();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoEquivalenciaProductos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public void deleteEquivalenciaProductoPedido(String idUs,EquivalenciaProductoPedido eqPro) throws Exception {
		DAOTablaPedidos daoPedidos = new DAOTablaPedidos();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		DAOTablaEquivalenciaProductoPedido daoEquivalenciaProductos = new DAOTablaEquivalenciaProductoPedido();		

		try 
		{			
			this.conn=darConexion();
			daoPedidos.setConn(conn);
			Pedido pew=daoPedidos.darPedidoPorId(eqPro.getIdPedido());
			if(pew.getIdUsuario()!=null)
			{
				Usuario x;
				if(!idUs.equals("0")) 
				{
					this.conn=darConexion();
					daoUsuarios.setConn(conn);
					x=daoUsuarios.buscarUsuarioPorId(idUs);			
					if(!x.getRol().equals("UsuarioCliente")) {
						throw new Exception("El identificador "+idUs+" no corresponde a un usuarioCliente");
					}
					if(!pew.getIdUsuario().equals(idUs))
					{
						throw new Exception ("El pedido con id"+eqPro.getIdPedido()+" s�lo puede ser editado por el usuario con id "+pew.getIdUsuario());

					}
				}
				else
				{
					throw new Exception ("El pedido con id"+eqPro.getIdPedido()+" s�lo puede ser editado por el usuario con id "+pew.getIdUsuario());
				}
			}


			//////transaccion
			this.conn = darConexion();

			daoEquivalenciaProductos.setConn(conn);
			daoEquivalenciaProductos.deleteEquivalenciaProductoPedido(eqPro);
			conn.commit();


		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoPedidos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public void addEquivalenciaIngredientePedido(String idUs, EquivalenciaIngredientePedido eqPro) throws Exception {

		DAOTablaEquivalenciaIngredientePedido daoEquivalenciaIngredientes = new DAOTablaEquivalenciaIngredientePedido();		
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		DAOTablaProductoPedido daoProductoPedidos = new DAOTablaProductoPedido();
		DAOTablaProductoIngrediente daoProdIng= new DAOTablaProductoIngrediente();
		DAOTablaEquivalenciaIngrediente daoEqPros = new DAOTablaEquivalenciaIngrediente();
		DAOTablaPedidos daoPedidos = new DAOTablaPedidos();

		try 
		{			
			this.conn=darConexion();
			daoPedidos.setConn(conn);
			Pedido pew=daoPedidos.darPedidoPorId(eqPro.getIdPedido());
			if(pew.getIdUsuario()!=null)
			{
				Usuario x;
				if(!idUs.equals("0")) 
				{
					this.conn=darConexion();
					daoUsuarios.setConn(conn);
					x=daoUsuarios.buscarUsuarioPorId(idUs);			
					if(!x.getRol().equals("UsuarioCliente")) {
						throw new Exception("El identificador "+idUs+" no corresponde a un usuarioCliente");
					}
					if(!pew.getIdUsuario().equals(idUs))
					{
						throw new Exception ("El pedido con id"+eqPro.getIdPedido()+" s�lo puede ser editado por el usuario con id "+pew.getIdUsuario());

					}
				}
				else
				{
					throw new Exception ("El pedido con id"+eqPro.getIdPedido()+" s�lo puede ser editado por el usuario con id "+pew.getIdUsuario());
				}
			}
			//////transaccion
			System.out.println("ola");

			this.conn=darConexion();
			daoProductoPedidos.setConn(conn);
			List<ProductoPedido> listaProductosActuales=daoProductoPedidos.darRestauranteProductosPorIdPedido(eqPro.getIdPedido());
			System.out.println("El producto tiene "+listaProductosActuales.size());
			if(listaProductosActuales==null||listaProductosActuales.isEmpty())
			{
				throw new Exception("No hay productos en el pedido con id "+eqPro.getIdPedido());				
			}
			boolean varz=false;
			for(ProductoPedido z:listaProductosActuales)
			{
				if(z.getIdPedido()==eqPro.getIdPedido()&&z.getNomProducto().equals(eqPro.getNomProducto())&&z.getNomRestaurante().equals(eqPro.getNomRestaurante()))
					varz=true;
			}

			if(!varz)
			{
				throw new Exception("La equivalencia se refiere a "+eqPro.getNomProducto()+", que no est� incluido en el pedido. Para hacer intercambiar un ingrediente del producto por uno equivalente, el producto que desea cambiar debe estar en el pedido");
			}

			this.conn=darConexion();
			daoEqPros.setConn(conn);
			List<EquivalenciaIngrediente> listaEq=daoEqPros.darEquivalenciasPorIngredienteEnRestaurante(eqPro.getNomIngrediente(), eqPro.getNomRestaurante());
			if(listaEq==null||listaEq.isEmpty())
			{
				throw new Exception ("El Ingrediente "+eqPro.getNomIngrediente()+" no es equivalente a ning�n otro en el restaurante "+eqPro.getNomRestaurante());
			}

			boolean varz2=false;
			for(EquivalenciaIngrediente q:listaEq)
			{
				if(q.getnomEquivalencia().equals(eqPro.getNomEquivalencia())&&q.getNomIngrediente().equals(eqPro.getNomIngrediente())&&q.getNomRestaurante().equals(eqPro.getNomRestaurante()))
					varz2=true;
			}
			if(!varz2)
			{
				throw new Exception ("La equivalencia "+eqPro.getNomEquivalencia()+" no puede ser aplicada para "+eqPro.getNomIngrediente()+" en "+eqPro.getNomRestaurante());
			}			


			this.conn = darConexion();
			daoEquivalenciaIngredientes.setConn(conn);
			daoEquivalenciaIngredientes.addEquivalenciaIngredientePedido(eqPro);
			conn.commit();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoEquivalenciaIngredientes.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public void deleteEquivalenciaIngredientePedido(String idUs,EquivalenciaIngredientePedido eqPro) throws Exception {
		DAOTablaPedidos daoPedidos = new DAOTablaPedidos();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		DAOTablaEquivalenciaIngredientePedido daoEquivalenciaIngredientes = new DAOTablaEquivalenciaIngredientePedido();		

		try 
		{			
			this.conn=darConexion();
			daoPedidos.setConn(conn);
			Pedido pew=daoPedidos.darPedidoPorId(eqPro.getIdPedido());
			if(pew.getIdUsuario()!=null)
			{
				Usuario x;
				if(!idUs.equals("0")) 
				{
					this.conn=darConexion();
					daoUsuarios.setConn(conn);
					x=daoUsuarios.buscarUsuarioPorId(idUs);			
					if(!x.getRol().equals("UsuarioCliente")) {
						throw new Exception("El identificador "+idUs+" no corresponde a un usuarioCliente");
					}
					if(!pew.getIdUsuario().equals(idUs))
					{
						throw new Exception ("El pedido con id"+eqPro.getIdPedido()+" s�lo puede ser editado por el usuario con id "+pew.getIdUsuario());

					}
				}
				else
				{
					throw new Exception ("El pedido con id"+eqPro.getIdPedido()+" s�lo puede ser editado por el usuario con id "+pew.getIdUsuario());
				}
			}


			//////transaccion
			this.conn = darConexion();

			daoEquivalenciaIngredientes.setConn(conn);
			daoEquivalenciaIngredientes.deleteEquivalenciaIngredientePedido(eqPro);
			conn.commit();


		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoPedidos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public RFC2 consultarUnaZona(Long id) throws Exception {

		DAOTablaZonas daoZona = new DAOTablaZonas();
		DAOTablaPedidos daoPedido = new DAOTablaPedidos();

		try{

			this.conn = darConexion();
			daoZona.setConn(conn);
			Zona zona= daoZona.buscarZonaPorId(id);

			this.conn = darConexion();
			daoPedido.setConn(conn);
			List<Pedido> pedidos =daoPedido.darPedidosPorIdZona(id);			

			return new RFC2(zona, pedidos);

		}catch (SQLException e) {
			System.err.println("SQLException:"+ e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoZona.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}

	}

	public InfoUsuario RFC3(String idCliente) throws Exception
	{
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		DAOTablaPedidos daoPedidos=new DAOTablaPedidos();
		DAOTablaPreferencias daoPref=new DAOTablaPreferencias();
		try 
		{
			//////transaccion
			this.conn = darConexion();
			daoUsuarios.setConn(conn);
			Usuario u=daoUsuarios.buscarUsuarioPorId(idCliente);
			if(u.getRol()!="UsuarioCliente")
				throw new Exception("El identificador "+idCliente+" no corresponde a un cliente");

			this.conn=darConexion();
			daoPref.setConn(conn);
			ArrayList<Preferencia> l=daoPref.darPreferencias(idCliente);

			this.conn=darConexion();
			daoPedidos.setConn(conn);
			ArrayList<Pedido> p=daoPedidos.darPedidoPorIdCliente(idCliente);

			return new InfoUsuario(u,l,p);

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoPedidos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}		

	}

	public List<FechaNombreCuenta> RFC6(String fechasita) throws SQLException
	{
		DAOTablaProductos daoProds=new DAOTablaProductos();
		try 
		{
			char[] n=fechasita.toCharArray();
			char[] de=new char[n.length+2];
			de[0]=n[0]; de[1]=n[1]; de[2]=n[2];de[3]=n[3];de[4]='/';de[5]=n[4];de[6]=n[5];de[7]='/';de[8]=n[6];de[9]=n[7];
			String mia="";
			for(int i=0;i<de.length;i++)
			{
				mia=mia+de[i];				
			}

			ArrayList<FechaNombreCuenta> def=new ArrayList();
			//////transaccion
			this.conn = darConexion();
			daoProds.setConn(conn);
			ArrayList<FechaNombreCuenta> u=daoProds.darProductosMasVendidos();	
			for(FechaNombreCuenta b:u)
			{
				if(b.getFecha().equals(mia))
					def.add(b);
			}
			return def;

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoProds.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}		
	}

	public List<InfoZonaInfoPedido> consultarUnaZona(String criterioBusqueda) throws Exception {

		DAOTablaZonas daoZona = new DAOTablaZonas();
		List<InfoZonaInfoPedido> zonass;

		try{
			this.conn = darConexion();
			daoZona.setConn(conn);
			zonass = daoZona.consultarUnaZona(criterioBusqueda);

		}catch (SQLException e) {
			System.err.println("SQLException:"+ e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoZona.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return zonass;


	}

	public List<Producto> buscarProductosMasOfrecidosRFC4() throws Exception {

		List<String> productos;
		ArrayList<Producto> losProd=new ArrayList();

		DAOTablaProductoMenu daoPM = new DAOTablaProductoMenu();

		try{
			this.conn = darConexion();
			daoPM.setConn(conn);
			productos = daoPM.darProductosMasOfrecidos();
			for(String p:productos)
			{
				Producto z=buscarProductoPorName(p);
				if(z!=null)
					losProd.add(z);
			}
			return losProd;

		}catch (SQLException e) {
			System.err.println("SQLException:"+ e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoPM.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}


	}

	public List<Producto> consultarProductosQueOfrecen(String criterio) throws Exception
	{
		List <Producto> rta;
		DAOTablaProductos daoP = new DAOTablaProductos();

		try 
		{
			//////transaccion
			this.conn = darConexion();
			daoP.setConn(conn);
			rta = daoP.consultarProductosQueOfrecen(criterio);

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoP.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return rta;

	}
	
	public void addPedido2 (List<Pedido> Pedidos) throws Exception {

		DAOTablaPedidos daoPedidos = new DAOTablaPedidos();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();

		try 
		{			
			//////transaccion

			for (int i = 0; i <Pedidos.size(); i++) {

				if(Pedidos.get(i).getIdUsuario()!=null)
				{
					this.conn=darConexion();
					daoUsuarios.setConn(conn);
					Usuario u = daoUsuarios.buscarUsuarioPorId(Pedidos.get(i).getIdUsuario());
					if(!u.getRol().equals("UsuarioCliente"))
						throw new Exception("El identificador "+Pedidos.get(i).getIdUsuario()+" no corresponde a un cliente");
				}

				if (darMesaPorId(Pedidos.get(i).getNumMesa()) == null) {

					throw new Exception("La mesa asignada al pedido con id" + Pedidos.get(i).getIdPedido()+ " no existe");
				}

			} 
			this.conn = darConexion();
			daoPedidos.setConn(conn);
			daoPedidos.RF15((ArrayList)Pedidos);
			conn.commit();
		}
		catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage()+"Error prueba 1");
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage()+ "Error prueba2");
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoPedidos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public RFC1 consultarRestaurante(String categoria) throws Exception 

	{
		DAOTablaRestauranteProducto daoRP = new DAOTablaRestauranteProducto();
		DAOTablaRestaurantes daoR = new DAOTablaRestaurantes();
		DAOTablaProductos daoP = new DAOTablaProductos();
		List<RestauranteProducto> resta = null;
		try{


			this.conn = darConexion();
			daoP.setConn(conn);
			List<Producto> producto = daoP.productoPorCategoria(categoria);


			for (int i = 0; i < producto.size(); i++) {

				this.conn = darConexion();
				daoRP.setConn(conn);
				resta = daoRP.darRestaurantesPorProducto(producto.get(i).getNombre());

			}

			return new RFC1(resta, producto);}

		catch (SQLException e) {
			System.err.println("SQLException:"+ e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoRP.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
			try {
				daoR.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} 
			catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}

	}

	public List<RestauranteProducto> RF13 (String idUsuario, String nameRestaurante) throws Exception
	{
		DAOTablaRestauranteProducto daoRestauranteProducto = new DAOTablaRestauranteProducto();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{		
			Usuario user;

			if(idUsuario.equals("0")) 
			{
				throw new Exception("S�lo un usuario 'usuarioRestaurante' puede editar los productos de un restaurante.");
			}
			else {
				this.conn = darConexion();
				daoUsuarios.setConn(conn);
				user = daoUsuarios.buscarUsuarioPorId(idUsuario);		

				if(!user.getRol().equals("UsuarioRestaurante")) {
					throw new Exception("El identificador " +idUsuario+" no corresponde a un usuario 'usuarioRestaurante'.");
				}
			}

			List<RestauranteProducto> productos = darProductosPorRestaurante(nameRestaurante);

			if(productos.size()==0)
			{
				throw new Exception("El restaurante "+ (nameRestaurante)+" no tiene ning�n producto.");
			}

			Restaurante rest = buscarRestaurantePorName(nameRestaurante);
			if(rest == null)
			{
				throw new Exception("El restaurante "+(nameRestaurante)+" no est� registrado en el sistema.");
			}

			this.conn = darConexion();
			daoRestauranteProducto.setConn(conn);
			daoRestauranteProducto.updateCantidad(nameRestaurante);
			conn.commit();


		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoRestauranteProducto.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}

		return darProductosPorRestaurante(nameRestaurante);
	}

	public List<Pedido> pedidosPorMesa(int numMesa) throws Exception {

		DAOTablaPedidos daoPedidos = new DAOTablaPedidos();
		List<Pedido> pedidos;
		try 
		{		

			if (darMesaPorId(numMesa)== null)
			{
				throw new Exception ("Esta mesa no est� registrada.");
			}
			this.conn = darConexion();
			daoPedidos.setConn(conn);
			pedidos = daoPedidos.darPedidosPorMesa2(numMesa);
			if (pedidos.size()==0)
			{
				throw new Exception ("No hay ning�n pedido para esta mesa.");
			}

			this.conn = darConexion();
			daoPedidos.setConn(conn);
			pedidos = daoPedidos.darPedidosPorMesa2(numMesa);
			conn.commit();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoPedidos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}

		return pedidos;


	}

	public Zona zonaPorId(Long idZona) throws Exception {

		DAOTablaZonas daoZonas = new DAOTablaZonas();
		Zona z;
		try 
		{
			//////transaccion
			this.conn = darConexion();
			daoZonas.setConn(conn);
			z = daoZonas.buscarZonaPorId(idZona);

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoZonas.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return z;
	}	

	public void RF15(String idUsuario, List<Pedido> listaPedidos) throws Exception {
		// TODO Auto-generated method stub

		Mesa mesa = null;

		if (listaPedidos.size()== 0)
		{
			throw new Exception("No se ingres� ning�n pedido.");
		}

		for (Pedido p:listaPedidos) {

			if (p==null)
			{
				throw new Exception("No se ingres� ning�n pedido");
			}
			Pedido pe = p;
			mesa = darMesaPorId(pe.getNumMesa());

			/*if (darMesaPorId(p.getNumMesa())!=null)
			{
				if (pedidosPorMesa(p.getNumMesa()).size() == mesa.getCapacidad() )
				{
					throw new Exception("La cantidad m�xima de pedidos para esta mesa ha sido alcanzada.");
			 */


		}
		addPedido2(listaPedidos);

	}
	
	public List<Pedido> RF16 (int mesa) throws Exception {

		DAOTablaPedidos dao = new DAOTablaPedidos();
		List<Pedido> pedidosMesa = null;
		DAOTablaMesa daoMesas = new DAOTablaMesa();
		try 
		{
			//////transaccion

			this.conn = darConexion();
			daoMesas.setConn(conn);
			if(daoMesas.darMesaPorId(mesa)!=null)
			{
				this.conn = darConexion();
				dao.setConn(conn);
				dao.RF16(mesa);
				pedidosMesa = dao.darPedidosPorMesa2(mesa);
			}
			else
			{
				throw new Exception ("La mesa "+ mesa+ " no existe.");
			}
			conn.commit();
		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoMesas.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return pedidosMesa;
	}

	public Mesa darMesaPorId(int numMesa) throws SQLException, Exception {
		// TODO Auto-generated method stub

		Mesa mesa;
		DAOTablaMesa daoMesas = new DAOTablaMesa();
		try 
		{
			//////transaccion
			this.conn = darConexion();
			daoMesas.setConn(conn);
			if(daoMesas.darMesaPorId(numMesa)!=null)
			{
				mesa = daoMesas.darMesaPorId(numMesa);
			}
			else
			{
				throw new Exception ("La mesa "+ numMesa+ " no existe.");
			}

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoMesas.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return mesa;
	}
	public void addMesa(String idUsuario, Mesa mesa) throws SQLException, Exception {

		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		DAOTablaMesa daoMesas = new DAOTablaMesa();
		DAOTablaZonas daoZ = new DAOTablaZonas();
		try 
		{		
			Usuario user;

			if(idUsuario.equals("0")) 
			{
				throw new Exception("S�lo un usuario 'usuarioRestaurante' puede agregar mesas.");
			}
			else {
				this.conn = darConexion();
				daoUsuarios.setConn(conn);
				user = daoUsuarios.buscarUsuarioPorId(idUsuario);		

				if(!user.getRol().equals("UsuarioRestaurante")) {
					throw new Exception("El identificador " +idUsuario+" no corresponde a un usuario 'usuarioRestaurante'.");
				}
			}
			
			this.conn = darConexion();
			daoMesas.setConn(conn);
			if (daoMesas.darMesaPorId(mesa.getNumMesa())!= null)
			{
				throw new Exception("Ya existe una mesa con ese n�mero.");
			}
			this.conn = darConexion();
			daoZ.setConn(conn);
			if(daoZ.buscarZonaPorId(mesa.getIdZona())!= null)
			{
			//////transaccion
			this.conn = darConexion();
			daoMesas.setConn(conn);
			Zona z = buscarZonaPorId(mesa.getIdZona());
			List <Mesa> mesaz = darMesas();
			int capacidadActual = 0;

			for (int i = 0; i < mesaz.size(); i++) {

				if (mesaz.get(i).getIdZona() == z.getId())
				{
					capacidadActual += mesaz.get(i).getCapacidad();
				}
			}

			if (capacidadActual>= z.getCapacidad())
			{
				throw new Exception("La zona no puede agregar esta mesa.");
			}
			}
			else
			{
				throw new Exception("La zona indicada no existe.");
			}
			this.conn = darConexion();
			daoMesas.setConn(conn);
			daoMesas.addMesa(mesa);
			conn.commit();

		} catch (SQLException e) {
			System.err.println("SQLException:"+ e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoMesas.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}	
	}
	
	public void addMesa(Mesa mesa) throws SQLException, Exception {

		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		DAOTablaMesa daoMesas = new DAOTablaMesa();
		DAOTablaZonas daoZ = new DAOTablaZonas();
		try 
		{	this.conn = darConexion();
			daoMesas.setConn(conn);
			daoMesas.addMesa(mesa);
			conn.commit();

		} catch (SQLException e) {
			System.err.println("SQLException:"+ e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoMesas.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}	
	}
	public Pedido cancelarPedido(String idUsuario, Pedido pPedido) throws Exception{

		DAOTablaUsuarios tablaUsuarios = new DAOTablaUsuarios();
		DAOTablaPedidos tablaPedidos = new DAOTablaPedidos();
		DAOTablaRestauranteProducto tablaRestauranteProducto = new DAOTablaRestauranteProducto();
		DAOTablaProductoPedido tablaProductoPedido =  new DAOTablaProductoPedido();
		try 
		{			
			this.conn = darConexion();
			tablaPedidos.setConn(conn);
			tablaRestauranteProducto.setConn(conn);
			tablaUsuarios.setConn(conn);
			tablaProductoPedido.setConn(conn);

			Usuario usuario = tablaUsuarios.buscarUsuarioPorId(""+idUsuario);
			if(!usuario.getRol().equals("UsuarioRestaurante") || usuario == null){
				throw new Exception("El usuario con id" + idUsuario + " no puede cancelar el pedido porque no es un restaurante");
			}else{
				Pedido pedido = tablaPedidos.darPedidoPorId(pPedido.getIdPedido());
				if(pedido.isServido()){
					throw new Exception("No se puede cancelar el pedido con id: " + pedido.getIdPedido() + "porque ya fue servido");
				}else{
					if(pedido.isCancelado()){
						throw new Exception("El pedido con id: " + pedido.getIdPedido() + " ya hab�a sido cancelado");
					}else{

						ArrayList<Producto> productos = tablaProductoPedido.getProductoFromPedido(pedido);

						tablaPedidos.servirPedido(pedido.getIdPedido());

						//Sumar los productos
						for(int i=0; i<productos.size(); i++){
							Producto pi = productos.get(i);
							tablaRestauranteProducto.sumarProducto(pi.getNombre());
						}

						Pedido respuesta = tablaPedidos.darPedidoPorId(pedido.getIdPedido());
						conn.commit();
						return respuesta;
					}
				}
			}
		}catch (SQLException e) {
			System.err.println("SQLException:"+ e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				tablaUsuarios.cerrarRecursos();
				tablaPedidos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
			try {
				tablaPedidos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} 
			catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}
	public List<Pedido> servir2(String idUsuario, Pedido pPedido) throws Exception {

		DAOTablaUsuarios tablaUsuarios = new DAOTablaUsuarios();
		DAOTablaPedidos tablaPedidos = new DAOTablaPedidos();
		DAOTablaRestauranteProducto tablaRestauranteProducto = new DAOTablaRestauranteProducto();
		DAOTablaProductoPedido tablaProductoPedido =  new DAOTablaProductoPedido();

		try 
		{			
			this.conn = darConexion();
			tablaPedidos.setConn(conn);
			tablaRestauranteProducto.setConn(conn);
			tablaUsuarios.setConn(conn);
			tablaProductoPedido.setConn(conn);
			ArrayList rta = new  ArrayList();
			Usuario usuario = tablaUsuarios.buscarUsuarioPorId(idUsuario);
			if(!usuario.getRol().equals("UsuarioRestaurante")){
				throw new Exception("El usuario con id" + idUsuario + " no puede servir el pedido porque no es un restaurante");
			}else{
				Pedido pedido = tablaPedidos.darPedidoPorId(pPedido.getIdPedido());

				rta = tablaPedidos.RF16(pedido.getNumMesa());
			}

			return rta;
		}catch (SQLException e) {
			System.err.println("SQLException:"+ e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				tablaPedidos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
			try {
				tablaPedidos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} 
			catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public void updateMesa(String idUs, Mesa mesa) throws SQLException, Exception {

		DAOTablaMesa daoMesas = new DAOTablaMesa();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{			
			Usuario x;
			if(!idUs.equals("0")) 
			{
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				x=daoUsuarios.buscarUsuarioPorId(idUs);			
				if(!x.getRol().equals("UsuarioRestaurante")) {
					throw new Exception("El identificador "+idUs+" no corresponde a un usuarioRestaurante");
				}
			}
			//////transaccion
			this.conn = darConexion();
			daoMesas.setConn(conn);
			daoMesas.updateMesa(mesa);

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoMesas.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public void deleteMesa(String idUsuario, Mesa mesa) throws SQLException, Exception {

		DAOTablaMesa daoMesas = new DAOTablaMesa();
		DAOTablaUsuarios daoUsuarios = new DAOTablaUsuarios();
		try 
		{			
			Usuario x;
			if(!idUsuario.equals("0")) 
			{
				this.conn=darConexion();
				daoUsuarios.setConn(conn);
				x=daoUsuarios.buscarUsuarioPorId(idUsuario);			
				if(!x.getRol().equals("UsuarioRestaurante")) {
					throw new Exception("El identificador "+idUsuario+" no corresponde a un usuarioRestaurante");
				}
			}
			//////transaccion
			this.conn = darConexion();
			daoMesas.setConn(conn);
			daoMesas.deleteMesa(mesa);

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoMesas.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}

	}

	public List<Mesa> darMesas() throws SQLException, Exception {

		List<Mesa> mesas;
		DAOTablaMesa daoMesas = new DAOTablaMesa();
		try 
		{			
			this.conn = darConexion();
			daoMesas.setConn(conn);
			mesas = daoMesas.darMesas();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				daoMesas.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return mesas;
	}
	public Pedido servirPedido(String idUsuario, Pedido pPedido) throws Exception{
		DAOTablaUsuarios tablaUsuarios = new DAOTablaUsuarios();
		DAOTablaPedidos tablaPedidos = new DAOTablaPedidos();
		DAOTablaRestauranteProducto tablaRestauranteProducto = new DAOTablaRestauranteProducto();
		DAOTablaProductoPedido tablaProductoPedido =  new DAOTablaProductoPedido();
		try 
		{			
			this.conn = darConexion();
			tablaPedidos.setConn(conn);
			tablaRestauranteProducto.setConn(conn);
			tablaUsuarios.setConn(conn);
			tablaProductoPedido.setConn(conn);

			Usuario usuario = tablaUsuarios.buscarUsuarioPorId(""+idUsuario);
			if(!usuario.getRol().equals("UsuarioRestaurante")){
				throw new Exception("El usuario con id" + idUsuario + " no puede servir el pedido porque no es un restaurante");
			}else{
				Pedido pedido = tablaPedidos.darPedidoPorId(pPedido.getIdPedido());
				if(pedido.isCancelado()){
					throw new Exception("No se puede servir el pedido con id: " + pedido.getIdPedido() + " porque fue cancelado");
				}else{
					if(pedido.isServido()){
						throw new Exception("El pedido con id: " + pedido.getIdPedido() + " ya hab�a sido servido");
					}else{


						tablaPedidos.servirPedido(pedido.getIdPedido());

						Pedido respuesta = tablaPedidos.darPedidoPorId(pedido.getIdPedido());
						conn.commit();
						return respuesta;
					}
				}
			}
		}catch (SQLException e) {
			System.err.println("SQLException:"+ e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				tablaPedidos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
			try {
				tablaPedidos.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} 
			catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}



	public ArrayList<RFC7> darPedidosConsolidados(String idUsuario) throws Exception{

		DAORFC7 tablaConsultas = new DAORFC7();
		DAOTablaUsuarios tablaUsuarios = new DAOTablaUsuarios();
		try 
		{	
			this.conn = darConexion();
			tablaConsultas.setConn(conn);
			tablaUsuarios.setConn(conn);
			Usuario user = tablaUsuarios.buscarUsuarioPorId(idUsuario);
			ArrayList<RFC7> pedidos = new ArrayList<>();
			if(user.getRol().equals("UsuarioAdmin")){
				pedidos = tablaConsultas.getPedidosUsuariosConsolidados();
			}else if(user.getRol().equals("UsuarioCliente")){
				pedidos = tablaConsultas.getPedidosUsuarioConsolidado(user.getIdentificacion());
			}else if(user == null || user.getRol().equals("UsuarioRestaurante")){
				throw new Exception("Para realizar est� acci�n debe tener privilegios de cliente");
			}
			return pedidos;

		}catch (SQLException e) {
			System.err.println("SQLException:"+ e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
			try {
				if(this.conn!=null)
					this.conn.close();
			} 
			catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public RFC8Consolidador consolidarPedidos(String idUsuario) throws Exception{
		DAORFC8 tablaConsulta = new DAORFC8();
		DAOTablaUsuarios tablaUsuarios =  new DAOTablaUsuarios();
		try 
		{
			this.conn = darConexion();
			tablaConsulta.setConn(conn);
			tablaUsuarios.setConn(conn);
			Usuario user = tablaUsuarios.buscarUsuarioPorId(idUsuario);

			ArrayList<RFC8Registrado> listaRegistrados = new ArrayList<>();
			ArrayList<RFC8NoRegistrado> listaNoRegistrados = new ArrayList<>();
			if(user.getRol().equals("UsuarioAdmin")){
				listaRegistrados = tablaConsulta.darConsolidadoUsuariosRegistrados();
				listaNoRegistrados = tablaConsulta.darConsolidadoUsuariosNoRegistrados();
				RFC8Consolidador consolidador =  new RFC8Consolidador(listaRegistrados, listaNoRegistrados);
				return consolidador;
			}else if(user.getRol().equals("UsuarioRestaurante")){
				throw new Exception("Ingrese el nombre del restaurante que desea consultar luego de su ID");

			}else{
				throw new Exception("No tiene permisos de restaurante para realizar esta consulta");
			}

		}catch (SQLException e) {
			System.err.println("SQLException:"+ e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
			try {
				if(this.conn!=null)
					this.conn.close();
			} 
			catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	public RFC8Consolidador consolidarPedidos(String idUsuario, String nomRestaurante) throws Exception{
		DAORFC8 tablaConsulta = new DAORFC8();
		DAOTablaUsuarios tablaUsuarios =  new DAOTablaUsuarios();
		DAOTablaRestaurantes tablaRestaurantes = new DAOTablaRestaurantes();
		try 
		{
			this.conn = darConexion();
			tablaConsulta.setConn(conn);
			tablaUsuarios.setConn(conn);
			tablaRestaurantes.setConn(conn);
			Usuario user = tablaUsuarios.buscarUsuarioPorId(idUsuario);
			Restaurante restaurante = tablaRestaurantes.buscarRestaurantePorName(nomRestaurante);
			ArrayList<RFC8Registrado> listaRegistrados = new ArrayList<>();
			ArrayList<RFC8NoRegistrado> listaNoRegistrados = new ArrayList<>();
			if(restaurante ==  null){
				throw new Exception ("El restaurante: " + nomRestaurante + " no existe");
			}else{
				listaRegistrados = tablaConsulta.darConsolidadoUsuariosRegistradosRestaurante(nomRestaurante);
				listaNoRegistrados = tablaConsulta.darConsolidadoUsuariosNoRegistradosRestaurante(nomRestaurante);
				RFC8Consolidador respuesta = new RFC8Consolidador(listaRegistrados, listaNoRegistrados);
				return respuesta;
			}

		}catch (SQLException e) {
			System.err.println("SQLException:"+ e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
			try {
				if(this.conn!=null)
					this.conn.close();
			} 
			catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}
	
	public void generarProductos() throws Exception
	{
		String[] categ= new String[5];
		categ[0]="Entrada";categ[1]="Postre";categ[2]="Acompanamiento";categ[3]="Bebida";categ[4]="Plato fuerte";
		
		String[] tipos=new String[9];
		tipos[0]="Casero";tipos[1]="Natural";tipos[2]="Comida rapida";tipos[3]="Colombiano";tipos[4]="Mexicano";
		tipos[5]="Europeo";tipos[6]="Asiatico";tipos[7]="Gourmet";tipos[8]="Italiano";
		try 
		{
			for(int i=1;i<=10000;i++)
			{
				Random generator = new Random();
				int indexCa = generator.nextInt(4);
				int indexTi= generator.nextInt(8);
				
				String nombreP="producto"+i;
				String descP="descripcion"+i;
				String descTradP="descTraducida"+i;
				int timeP=generator.nextInt(60);
				String tipoP=tipos[indexTi];
				String cateP=categ[indexCa];
				
				Producto p=new Producto(nombreP,descP,descTradP,timeP,tipoP,cateP);
				addProducto(p);
				
			}

		}catch (SQLException e) {
			System.err.println("SQLException:"+ e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
			try {
				if(this.conn!=null)
					this.conn.close();
			} 
			catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}

	}
	
	public void generarIngredientes() throws Exception
	{
		String[] categ= new String[5];
		categ[0]="Entrada";categ[1]="Postre";categ[2]="Acompanamiento";categ[3]="Bebida";categ[4]="Plato fuerte";
		
		String[] tipos=new String[9];
		tipos[0]="Casero";tipos[1]="Natural";tipos[2]="Comida rapida";tipos[3]="Colombiano";tipos[4]="Mexicano";
		tipos[5]="Europeo";tipos[6]="Asiatico";tipos[7]="Gourmet";tipos[8]="Italiano";
		try 
		{
			for(int i=1;i<=10000;i++)
			{
				Random generator = new Random();
				int indexCa = generator.nextInt(4);
				int indexTi= generator.nextInt(8);
				
				String nombreP="producto"+i;
				String descP="descripcion"+i;
				String descTradP="descTraducida"+i;
				Ingrediente p=new Ingrediente(nombreP,descP,descTradP);
				addIngrediente(p);
				
			}

		}catch (SQLException e) {
			System.err.println("SQLException:"+ e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
			try {
				if(this.conn!=null)
					this.conn.close();
			} 
			catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}

	}
	
	public void generarRestaurantes() throws Exception
	{
		
		String[] tipos=new String[9];
		tipos[0]="Casero";tipos[1]="Natural";tipos[2]="Comida rapida";tipos[3]="Colombiano";tipos[4]="Mexicano";
		tipos[5]="Europeo";tipos[6]="Asiatico";tipos[7]="Gourmet";tipos[8]="Italiano";
		try 
		{
			for(int i=1;i<=10000;i++)
			{
				Random generator = new Random();
				int indexCa = generator.nextInt(4);
				int indexTi= generator.nextInt(8);
				
				String nombreP="restaurante"+i;
				String rep="IDR"+i;
				String pa="paginaweb"+i;
				String tipoP=tipos[indexTi];
				
				Restaurante p=new Restaurante(nombreP,rep,tipoP,pa);
				addRestaurante(p);
				
			}

		}catch (SQLException e) {
			System.err.println("SQLException:"+ e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
			try {
				if(this.conn!=null)
					this.conn.close();
			} 
			catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}

	}
	
	public void generarZonas() throws Exception
	{
		try 
		{
			Zona rellenito=new Zona((long) 555,3500,true,true,true,true,true,true);
			addZona(rellenito);
			for(int i=1;i<=100;i++)
			{
				Long id=(long) (i+0);
				Random generator = new Random();
				int cap = generator.nextInt(150);				
				
				Zona p=new Zona(id,cap,generator.nextBoolean(),generator.nextBoolean(),generator.nextBoolean(),generator.nextBoolean(),generator.nextBoolean(),generator.nextBoolean());
				addZona(p);
				
			}
			
			

		}catch (SQLException e) {
			System.err.println("SQLException:"+ e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
			try {
				if(this.conn!=null)
					this.conn.close();
			} 
			catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}

	}
	
	public void generarUsuarios() throws Exception
	{
		String[] roles= new String[4];
		roles[0]="UsuarioAdmin";roles[1]="UsuarioCliente";roles[2]="UsuarioRestaurante";roles[3]="UsuarioCliente";
		try 
		{
			for(int i=1;i<=10000;i++)
			{
				Random generator = new Random();
				int indexTi= generator.nextInt(3);
				
				String nombreP="usuario"+i;
				String correo="correo"+i;
				String id="ID"+i;
				String r=roles[indexTi];
				
				Usuario p=new Usuario(nombreP,id,correo,r);
				addUsuario(p);
				
			}

		}catch (SQLException e) {
			System.err.println("SQLException:"+ e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
			try {
				if(this.conn!=null)
					this.conn.close();
			} 
			catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}

	}
	
	public void generarMesas() throws Exception
	{
		String[] roles= new String[4];
		roles[0]="UsuarioAdmin";roles[1]="UsuarioCliente";roles[2]="UsuarioRestaurante";roles[3]="UsuarioCliente";
		try 
		{
			for(int i=1;i<=10000;i++)
			{
				Random generator = new Random();
				int indexZona= generator.nextInt(100);
				Long laZona=(long)(indexZona);
				int cap=generator.nextInt(6);
				
				Mesa p=new Mesa(i,cap,laZona);
				addMesa(p);
				
			}

		}catch (SQLException e) {
			System.err.println("SQLException:"+ e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
			try {
				if(this.conn!=null)
					this.conn.close();
			} 
			catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}

	}
	
	public List<RFC9Simple> darConsumoUsuarios(String idUsuario, RestauranteFecha rf, String orden) throws Exception{
		DAORFC9 tablaConsulta = new DAORFC9();
		DAOTablaUsuarios tablaUsuarios = new DAOTablaUsuarios();

		try 
		{
			this.conn = darConexion();
			tablaConsulta.setConn(conn);
			tablaUsuarios.setConn(conn);
			Usuario usuario = tablaUsuarios.buscarUsuarioPorId(idUsuario);
			Long ti = System.currentTimeMillis();
			if(usuario.getRol().equals("usuarioAdmin")){
				ArrayList<RFC9Simple> respuesta = new ArrayList<>();
				String restaurante = rf.getRestaurante();
				String fechaInicial = rf.getFechaInicial();
				String fechaFinal = rf.getFechaFinal();
				if(orden.isEmpty()){
					respuesta = tablaConsulta.darConsumoUsuarios(restaurante, fechaInicial, fechaFinal);
				}else if(orden.equals("ordenUsuario")){
					respuesta = tablaConsulta.darConsumoUsuariosOrdenUsuario(restaurante, fechaInicial, fechaFinal);
				}else if(orden.equals("ordenProducto")){
					respuesta = tablaConsulta.darConsumoUsuariosOrdenProducto(restaurante, fechaInicial, fechaFinal);
				}else{
					throw new Exception("El m�todo de ordenamiento no es correcto");
				}
				Long tf = System.currentTimeMillis();
				Long tt = tf-ti;
				String tiempo = "El tiempo de consulta fue: " + tt + "ms";
				String log = "CONSULTA: RFC9 \n" + "ORDEN:" + orden + "\n" + "TIEMPO: " + tiempo;
				System.out.println(log);
				return respuesta;
			}else if(usuario.getRol().equals("UsuarioCliente")){
				ArrayList<RFC9Simple> respuesta = new ArrayList<>();
				String restaurante = rf.getRestaurante();
				String fechaInicial = rf.getFechaInicial();
				String fechaFinal = rf.getFechaFinal();
				if(orden.isEmpty()){
					respuesta = tablaConsulta.darConsumoUsuario(restaurante, fechaInicial, fechaFinal, idUsuario);
				}else if(orden.equals("ordenProducto")){
					respuesta = tablaConsulta.darConsumoUsuarioOrdenProducto(restaurante, fechaInicial, fechaFinal, idUsuario);
				}else{
					throw new Exception("El m�todo de ordenamiento no es correcto");
				}
				Long tf = System.currentTimeMillis();
				Long tt = tf-ti;
				String tiempo = "El tiempo de consulta fue: " + tt + "ms";
				String log = "CONSULTA: RFC9 \n" + "ORDEN:" + orden + "\n" + "TIEMPO: " + tiempo;
				System.out.println(log);
				return respuesta;
			}else{
				throw new Exception("ERROR: No tiene permisos para realizar esta acci�n \n");
			}

		}catch (SQLException e) {
			System.err.println("SQLException:"+ e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
			try {
				if(this.conn!=null)
					this.conn.close();
			} 
			catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}


	}

	public List<RFC11FechaProducto> darProductoMasConsumido(String idUsuario, FechaInicial fi) throws Exception{
		DAORFC11 tablaConsulta = new DAORFC11();
		DAOTablaUsuarios tablaUsuarios = new DAOTablaUsuarios();
		try 
		{
			this.conn = darConexion();
			tablaConsulta.setConn(conn);
			tablaUsuarios.setConn(conn);
			String dia = fi.getFechaInicial();
			Usuario user = tablaUsuarios.buscarUsuarioPorId(idUsuario);
			ArrayList<RFC11FechaProducto> resultado = new ArrayList<>();
			Long ti = System.currentTimeMillis();
			if(user.getRol().equals("usuarioAdmin")){
				for(int i=0; i < 7; i++){
					System.out.println(dia);
					List<RFC11FechaProducto> lista = tablaConsulta.darProductosMASConsumidos(dia);
					if(lista.isEmpty()){
						resultado.add(new RFC11FechaProducto(dia, 0, ""));
					}else{
						RFC11FechaProducto nuevo = buscarProductoMasConsumido(tablaConsulta.darProductosMASConsumidos(dia));
						resultado.add(nuevo);
					}
					dia = diaSiguiente(dia);
				}
				Long tf = System.currentTimeMillis();
				Long tt = tf-ti;
				String tiempo = "El tiempo de consulta fue: " + tt + "ms";
				String log = "CONSULTA: RFC11 \n" + "TIPO: Producto m�s consumido \n" + "TIEMPO: " + tiempo;
				System.out.println(log);
				return resultado;
			}else{
				throw new Exception("No tiene permisos para realizar esta consulta");
			}

		}catch (SQLException e) {
			System.err.println("SQLException:"+ e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
			try {
				if(this.conn!=null)
					this.conn.close();
			} 
			catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}
	
	public List<RFC11FechaProducto> darProductoMenosConsumido(String idUsuario, FechaInicial fi) throws Exception{
		DAORFC11 tablaConsulta = new DAORFC11();
		DAOTablaUsuarios tablaUsuarios = new DAOTablaUsuarios();
		try 
		{
			this.conn = darConexion();
			tablaConsulta.setConn(conn);
			tablaUsuarios.setConn(conn);
			String dia = fi.getFechaInicial();
			Usuario user = tablaUsuarios.buscarUsuarioPorId(idUsuario);
			ArrayList<RFC11FechaProducto> resultado = new ArrayList<>();
			Long ti = System.currentTimeMillis();
			if(user.getRol().equals("usuarioAdmin")){
				for(int i=0; i < 7; i++){
					List<RFC11FechaProducto> lista = tablaConsulta.darProductosMASConsumidos(dia);
					if(lista.isEmpty()){
						resultado.add(new RFC11FechaProducto(dia, 0, ""));
					}else{
						RFC11FechaProducto nuevo = buscarProductoMenosConsumido(tablaConsulta.darProductosMASConsumidos(dia));
						resultado.add(nuevo);
					}
					dia = diaSiguiente(dia);
				}
				Long tf = System.currentTimeMillis();
				Long tt = tf-ti;
				String tiempo = "El tiempo de consulta fue: " + tt + "ms";
				String log = "CONSULTA: RFC11 \n" + "TIPO: Producto menos consumido \n" + "TIEMPO: " + tiempo;
				System.out.println(log);
				return resultado;
			}else{
				throw new Exception("No tiene permisos para realizar esta consulta");
			}

		}catch (SQLException e) {
			System.err.println("SQLException:"+ e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
			try {
				if(this.conn!=null)
					this.conn.close();
			} 
			catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}
	
	public List<RFC11FechaRestaurante> darRestauranteMasFrecuentado(String idUsuario, FechaInicial fi) throws Exception{
		DAORFC11 tablaConsulta = new DAORFC11();
		DAOTablaUsuarios tablaUsuarios = new DAOTablaUsuarios();
		try 
		{
			this.conn = darConexion();
			tablaConsulta.setConn(conn);
			tablaUsuarios.setConn(conn);
			String dia = fi.getFechaInicial();
			Usuario user = tablaUsuarios.buscarUsuarioPorId(idUsuario);
			ArrayList<RFC11FechaRestaurante> resultado = new ArrayList<>();
			Long ti = System.currentTimeMillis();
			if(user.getRol().equals("usuarioAdmin")){
				for(int i=0; i < 7; i++){
					List<RFC11FechaRestaurante> lista = tablaConsulta.darRestaurantesMASFrecuentados(dia);
					if(lista.isEmpty()){
						resultado.add(new RFC11FechaRestaurante(dia, 0, ""));
					}else{
						RFC11FechaRestaurante nuevo = buscarRestauranteMasFrecuentado(lista);
						resultado.add(nuevo);
					}
					dia = diaSiguiente(dia);
				}
				Long tf = System.currentTimeMillis();
				Long tt = tf-ti;
				String tiempo = "El tiempo de consulta fue: " + tt + "ms";
				String log = "CONSULTA: RFC11 \n" + "TIPO: Restaurante m�s frecuentado \n" + "TIEMPO: " + tiempo;
				System.out.println(log);
				return resultado;
			}else{
				throw new Exception("No tiene permisos para realizar esta consulta");
			}

		}catch (SQLException e) {
			System.err.println("SQLException:"+ e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
			try {
				if(this.conn!=null)
					this.conn.close();
			} 
			catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}
	
	public List<RFC11FechaRestaurante> darRestauranteMenosFrecuentado(String idUsuario, FechaInicial fi) throws Exception{
		DAORFC11 tablaConsulta = new DAORFC11();
		DAOTablaUsuarios tablaUsuarios = new DAOTablaUsuarios();
		try 
		{
			this.conn = darConexion();
			tablaConsulta.setConn(conn);
			tablaUsuarios.setConn(conn);
			String dia = fi.getFechaInicial();
			Usuario user = tablaUsuarios.buscarUsuarioPorId(idUsuario);
			ArrayList<RFC11FechaRestaurante> resultado = new ArrayList<>();
			Long ti = System.currentTimeMillis();
			if(user.getRol().equals("usuarioAdmin")){
				for(int i=0; i < 7; i++){
					List<RFC11FechaRestaurante> lista = tablaConsulta.darRestaurantesMASFrecuentados(dia);
					if(lista.isEmpty()){
						resultado.add(new RFC11FechaRestaurante(dia, 0, ""));
					}else{
						RFC11FechaRestaurante nuevo = buscarRestauranteMenosFrecuentado(lista);
						resultado.add(nuevo);
					}
					dia = diaSiguiente(dia);
				}
				Long tf = System.currentTimeMillis();
				Long tt = tf-ti;
				String tiempo = "El tiempo de consulta fue: " + tt + "ms";
				String log = "CONSULTA: RFC11 \n" + "TIPO: Restaurante menos frecuentado \n" + "TIEMPO: " + tiempo;
				System.out.println(log);
				return resultado;
			}else{
				throw new Exception("No tiene permisos para realizar esta consulta");
			}

		}catch (SQLException e) {
			System.err.println("SQLException:"+ e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
			try {
				if(this.conn!=null)
					this.conn.close();
			} 
			catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}

	private String diaSiguiente(String dia) throws ParseException{
		Calendar calHoy = Calendar.getInstance();

		String pattern = "dd-mm-yyyy";
		Date dateHoy = new SimpleDateFormat(pattern).parse(dia);
		calHoy.setTime(dateHoy);

		calHoy.add(Calendar.DAY_OF_MONTH, 1);
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(calHoy.getTime());

	}

	private RFC11FechaProducto buscarProductoMasConsumido(List<RFC11FechaProducto> lista){
		RFC11FechaProducto mayor = lista.get(0);
		for(RFC11FechaProducto prod: lista){
			if(prod.getCantidad() > mayor.getCantidad()){
				mayor = prod;
			}
		}
		return mayor;
	}
	
	private RFC11FechaProducto buscarProductoMenosConsumido(List<RFC11FechaProducto> lista){
		RFC11FechaProducto menor = lista.get(0);
		for(RFC11FechaProducto prod: lista){
			if(prod.getCantidad() < menor.getCantidad()){
				menor = prod;
			}
		}
		return menor;
	}

	private RFC11FechaRestaurante buscarRestauranteMenosFrecuentado(List<RFC11FechaRestaurante> lista){
		RFC11FechaRestaurante menor = lista.get(0);
		for(RFC11FechaRestaurante prod: lista){
			if(prod.getCantidad() < menor.getCantidad()){
				menor = prod;
			}
		}
		return menor;
	}
	
	private RFC11FechaRestaurante buscarRestauranteMasFrecuentado(List<RFC11FechaRestaurante> lista){
		RFC11FechaRestaurante mayor = lista.get(0);
		for(RFC11FechaRestaurante prod: lista){
			if(prod.getCantidad() > mayor.getCantidad()){
				mayor = prod;
			}
		}
		return mayor;
	}
	
	


}

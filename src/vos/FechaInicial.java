package vos;

import org.codehaus.jackson.annotate.JsonProperty;

public class FechaInicial {
	
	@JsonProperty("fechaInicial")
	private String fechaInicial;
	
	public FechaInicial(@JsonProperty("fechaInicial") String fechaInicial){
		this.fechaInicial = fechaInicial;
	}

	public String getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(String fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
}

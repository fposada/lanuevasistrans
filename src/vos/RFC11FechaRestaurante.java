package vos;

import org.codehaus.jackson.annotate.JsonProperty;

public class RFC11FechaRestaurante {

	@JsonProperty("fecha")
	private String fecha;
	
	@JsonProperty("cantidad")
	private int cantidad;
	
	@JsonProperty("nomRestaurante")
	private String nomRestaurante;
	
	public RFC11FechaRestaurante(@JsonProperty("fecha")String fecha, @JsonProperty("cantidad") int cantidad, @JsonProperty("nomRestaurante") String nomRestaurante){
		this.cantidad = cantidad;
		this.nomRestaurante = nomRestaurante;
		this.fecha = fecha;
	}
	
	

	public String getFecha() {
		return fecha;
	}



	public void setFecha(String fecha) {
		this.fecha = fecha;
	}



	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getNomRestaurante() {
		return nomRestaurante;
	}

	public void setNomRestaurante(String nomRestaurante) {
		this.nomRestaurante = nomRestaurante;
	}
	
	
}

package vos;

import org.codehaus.jackson.annotate.JsonProperty;

public class RFC8NoRegistrado {
	
	@JsonProperty("idPedido")
	private Long idPedido;
	
	@JsonProperty("nomRestaurante")
	private String nomRestaurante;
	
	@JsonProperty("idZona")
	private Long idZona;
	
	@JsonProperty("numMesa")
	private Long numMesa;
	
	@JsonProperty("idUsuario")
	private String idUsuario;
	
	@JsonProperty("nomProducto")
	private String nomProducto;
	
	@JsonProperty("costoProd")
	private int costoProd;
	
	@JsonProperty("precio")
	private int precio;
	
	public RFC8NoRegistrado(@JsonProperty("idPedido") Long idPedido, @JsonProperty("nomRestaurante") String nomRestaurante,
			@JsonProperty("idZona") Long idZona, @JsonProperty("numMesa") Long numMesa, 
			@JsonProperty("idUsuario") String idUsuario, @JsonProperty("nomProducto") String nomProducto,
			@JsonProperty("costoProd") int costoProd, @JsonProperty("precio") int precio){
		
		this.idPedido = idPedido;
		this.nomRestaurante = nomRestaurante;
		this.idZona = idZona;
		this.numMesa = numMesa;
		this.idUsuario = idUsuario;
		this.nomProducto = nomProducto;
		this.costoProd = costoProd;
		this.precio = precio;
		
	}

	public Long getIdPedido() {
		return idPedido;
	}

	public void setIdPedido(Long idPedido) {
		this.idPedido = idPedido;
	}

	public String getNomRestaurante() {
		return nomRestaurante;
	}

	public void setNomRestaurante(String nomRestaurante) {
		this.nomRestaurante = nomRestaurante;
	}

	public Long getIdZona() {
		return idZona;
	}

	public void setIdZona(Long idZona) {
		this.idZona = idZona;
	}

	public Long getNumMesa() {
		return numMesa;
	}

	public void setNumMesa(Long numMesa) {
		this.numMesa = numMesa;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNomProducto() {
		return nomProducto;
	}

	public void setNomProducto(String nomProducto) {
		this.nomProducto = nomProducto;
	}

	public int getCostoProd() {
		return costoProd;
	}

	public void setCostoProd(int costoProd) {
		this.costoProd = costoProd;
	}

	public int getPrecio() {
		return precio;
	}

	public void setPrecio(int precio) {
		this.precio = precio;
	}
}
	
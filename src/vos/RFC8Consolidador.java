package vos;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

public class RFC8Consolidador {
	
	@JsonProperty("listaRegistrados")
	private List<RFC8Registrado> listaRegistrados;
	
	@JsonProperty("listaNoRegistrados")
	private List<RFC8NoRegistrado> listaNoRegistrados;
	
	public RFC8Consolidador(@JsonProperty("listaRegistrados") List<RFC8Registrado> listaRegistrados,
			@JsonProperty("listaNoRegistrados") List<RFC8NoRegistrado> listaNoRegistrados){
		this.listaRegistrados = listaRegistrados;
		this.listaNoRegistrados = listaNoRegistrados;
	}

	public List<RFC8Registrado> getListaRegistrados() {
		return listaRegistrados;
	}

	public void setListaRegistrados(List<RFC8Registrado> listaRegistrados) {
		this.listaRegistrados = listaRegistrados;
	}

	public List<RFC8NoRegistrado> getListaNoRegistrados() {
		return listaNoRegistrados;
	}

	public void setListaNoRegistrados(List<RFC8NoRegistrado> listaNoRegistrados) {
		this.listaNoRegistrados = listaNoRegistrados;
	}
}
	
	
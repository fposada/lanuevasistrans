package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import vos.RFC9Simple;

public class DAORFC9 {

	/**
	 * Arraylits de recursos que se usan para la ejecucion de sentencias SQL
	 */
	private ArrayList<Object> recursos;

	/**
	 * Atributo que genera la conexion a la base de datos
	 */
	private Connection conn;

	/**
	 * Metodo constructor que crea DAOTablaIngredientes
	 * <b>post: </b> Crea la instancia del DAO e inicializa el Arraylist de recursos
	 */
	public DAORFC9() {
		recursos = new ArrayList<Object>();
	}

	/**
	 * Metodo que cierra todos los recursos que estan enel arreglo de recursos
	 * <b>post: </b> Todos los recurso del arreglo de recursos han sido cerrados
	 */
	public void cerrarRecursos() {
		for(Object ob : recursos){
			if(ob instanceof PreparedStatement)
				try {
					((PreparedStatement) ob).close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
		}
	}

	/**
	 * Metodo que inicializa la connection del DAO a la base de datos con la conexion que entra como parametro.
	 * @param con  - connection a la base de datos
	 */
	public void setConn(Connection con){
		this.conn = con;
	}
	
	public ArrayList<RFC9Simple> darConsumoUsuarios(String restaurante, String fechaInicial, String fechaFinal) throws SQLException{
		ArrayList<RFC9Simple> respuesta = new ArrayList<>();
		
		String sql = "SELECT IDUSUARIO, CANTIDAD_PEDIDOS, NOMBRE, CORREO, ROL ";
		sql += "FROM(( ";
		sql += "SELECT P.IDUSUARIO, COUNT(P.IDPEDIDO) AS CANTIDAD_PEDIDOS ";
		sql += "FROM( ";
		sql += "SELECT PED.IDPEDIDO, PED.FECHA, PED.IDUSUARIO, PRODPED.NOMPRODUCTO, PRODPED.NOMRESTAURANTE ";
		sql += "FROM PEDIDO PED INNER JOIN PRODUCTOPEDIDO PRODPED ";
		sql += "ON PED.IDPEDIDO = PRODPED.IDPEDIDO ";
		sql += "WHERE NOMRESTAURANTE LIKE '" + restaurante + "' ";
		sql += "AND PED.FECHA > TO_DATE('" + fechaInicial + "','DD-MM-YYYY') ";
		sql += "AND PED.FECHA < TO_DATE('" + fechaFinal + "','DD-MM-YYYY')) P ";
		sql += "GROUP BY P.IDUSUARIO) A INNER JOIN USUARIO ";
		sql += "ON A.IDUSUARIO = USUARIO.IDENTIFICACION) ";
		sql += "WHERE CANTIDAD_PEDIDOS > 0 ";
		
		System.out.println("SENTENCIA: " + sql);
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			String idUsuario = rs.getString("IDUSUARIO");
			int cantidadPedidos = rs.getInt("CANTIDAD_PEDIDOS");
			String nombre = rs.getString("NOMBRE");
			String correo = rs.getString("CORREO");
			String rol = rs.getString("ROL");
			
			RFC9Simple nuevo = new RFC9Simple(idUsuario, cantidadPedidos, nombre, correo, rol);
			respuesta.add(nuevo);
		}
		
		return respuesta;
	}
	
	public ArrayList<RFC9Simple> darConsumoUsuariosOrdenUsuario(String restaurante, String fechaInicial, String fechaFinal) throws SQLException{
		ArrayList<RFC9Simple> respuesta = new ArrayList<>();
		
		String sql = "SELECT IDUSUARIO, CANTIDAD_PEDIDOS, NOMBRE, CORREO, ROL ";
		sql += "FROM(( ";
		sql += "SELECT P.IDUSUARIO, COUNT(P.IDPEDIDO) AS CANTIDAD_PEDIDOS ";
		sql += "FROM( ";
		sql += "SELECT PED.IDPEDIDO, PED.FECHA, PED.IDUSUARIO, PRODPED.NOMPRODUCTO, PRODPED.NOMRESTAURANTE ";
		sql += "FROM PEDIDO PED INNER JOIN PRODUCTOPEDIDO PRODPED ";
		sql += "ON PED.IDPEDIDO = PRODPED.IDPEDIDO ";
		sql += "WHERE NOMRESTAURANTE LIKE '" + restaurante + "' ";
		sql += "AND PED.FECHA > TO_DATE('" + fechaInicial + "','DD-MM-YYYY') ";
		sql += "AND PED.FECHA < TO_DATE('" + fechaFinal + "','DD-MM-YYYY')) P ";
		sql += "GROUP BY P.IDUSUARIO) A INNER JOIN USUARIO ";
		sql += "ON A.IDUSUARIO = USUARIO.IDENTIFICACION) ";
		sql += "WHERE CANTIDAD_PEDIDOS > 0 ";
		sql += "ORDER BY NOMBRE";
		
		System.out.println("SENTENCIA: " + sql);
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			String idUsuario = rs.getString("IDUSUARIO");
			int cantidadPedidos = rs.getInt("CANTIDAD_PEDIDOS");
			String nombre = rs.getString("NOMBRE");
			String correo = rs.getString("CORREO");
			String rol = rs.getString("ROL");
			
			RFC9Simple nuevo = new RFC9Simple(idUsuario, cantidadPedidos, nombre, correo, rol);
			respuesta.add(nuevo);
		}
		
		return respuesta;
	}
	
	public ArrayList<RFC9Simple> darConsumoUsuariosOrdenProducto(String restaurante, String fechaInicial, String fechaFinal) throws SQLException{
		ArrayList<RFC9Simple> respuesta = new ArrayList<>();
		
		String sql = "SELECT IDUSUARIO, CANTIDAD_PEDIDOS, NOMBRE, CORREO, ROL ";
		sql += "FROM(( ";
		sql += "SELECT P.IDUSUARIO, COUNT(P.IDPEDIDO) AS CANTIDAD_PEDIDOS ";
		sql += "FROM( ";
		sql += "SELECT PED.IDPEDIDO, PED.FECHA, PED.IDUSUARIO, PRODPED.NOMPRODUCTO, PRODPED.NOMRESTAURANTE ";
		sql += "FROM PEDIDO PED INNER JOIN PRODUCTOPEDIDO PRODPED ";
		sql += "ON PED.IDPEDIDO = PRODPED.IDPEDIDO ";
		sql += "WHERE NOMRESTAURANTE LIKE '" + restaurante + "' ";
		sql += "AND PED.FECHA > TO_DATE('" + fechaInicial + "','DD-MM-YYYY') ";
		sql += "AND PED.FECHA < TO_DATE('" + fechaFinal + "','DD-MM-YYYY')" + "ORDER BY PRODPED.NOMPRODUCTO) P ";
		sql += "GROUP BY P.IDUSUARIO) A INNER JOIN USUARIO ";
		sql += "ON A.IDUSUARIO = USUARIO.IDENTIFICACION) ";
		sql += "WHERE CANTIDAD_PEDIDOS > 0 ";
		
		System.out.println("SENTENCIA: " + sql);
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			String idUsuario = rs.getString("IDUSUARIO");
			int cantidadPedidos = rs.getInt("CANTIDAD_PEDIDOS");
			String nombre = rs.getString("NOMBRE");
			String correo = rs.getString("CORREO");
			String rol = rs.getString("ROL");
			
			RFC9Simple nuevo = new RFC9Simple(idUsuario, cantidadPedidos, nombre, correo, rol);
			respuesta.add(nuevo);
		}
		
		return respuesta;
	}
	
	//----------------------------------------------------------------------------------------
	// CONSUMO USUARIO INDIVIDUAL
	//----------------------------------------------------------------------------------------
	
	public ArrayList<RFC9Simple> darConsumoUsuario(String restaurante, String fechaInicial, String fechaFinal, String pIdUsuario) throws SQLException{
		ArrayList<RFC9Simple> respuesta = new ArrayList<>();
		
		String sql = "SELECT IDUSUARIO, CANTIDAD_PEDIDOS, NOMBRE, CORREO, ROL ";
		sql += "FROM(( ";
		sql += "SELECT P.IDUSUARIO, COUNT(P.IDPEDIDO) AS CANTIDAD_PEDIDOS ";
		sql += "FROM( ";
		sql += "SELECT PED.IDPEDIDO, PED.FECHA, PED.IDUSUARIO, PRODPED.NOMPRODUCTO, PRODPED.NOMRESTAURANTE ";
		sql += "FROM PEDIDO PED INNER JOIN PRODUCTOPEDIDO PRODPED ";
		sql += "ON PED.IDPEDIDO = PRODPED.IDPEDIDO ";
		sql += "WHERE NOMRESTAURANTE LIKE '" + restaurante + "' AND IDUSUARIO LIKE '" + pIdUsuario + "' ";
		sql += "AND PED.FECHA > TO_DATE('" + fechaInicial + "','DD-MM-YYYY') ";
		sql += "AND PED.FECHA < TO_DATE('" + fechaFinal + "','DD-MM-YYYY')) P ";
		sql += "GROUP BY P.IDUSUARIO) A INNER JOIN USUARIO ";
		sql += "ON A.IDUSUARIO = USUARIO.IDENTIFICACION) ";
		sql += "WHERE CANTIDAD_PEDIDOS > 0 ";
		
		System.out.println("SENTENCIA: " + sql);
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			String idUsuario = rs.getString("IDUSUARIO");
			int cantidadPedidos = rs.getInt("CANTIDAD_PEDIDOS");
			String nombre = rs.getString("NOMBRE");
			String correo = rs.getString("CORREO");
			String rol = rs.getString("ROL");
			
			RFC9Simple nuevo = new RFC9Simple(idUsuario, cantidadPedidos, nombre, correo, rol);
			respuesta.add(nuevo);
		}
		
		return respuesta;
	}
	
	public ArrayList<RFC9Simple> darConsumoUsuarioOrdenProducto(String restaurante, String fechaInicial, String fechaFinal, String pIdUsuario) throws SQLException{
		ArrayList<RFC9Simple> respuesta = new ArrayList<>();
		
		String sql = "SELECT IDUSUARIO, CANTIDAD_PEDIDOS, NOMBRE, CORREO, ROL ";
		sql += "FROM(( ";
		sql += "SELECT P.IDUSUARIO, COUNT(P.IDPEDIDO) AS CANTIDAD_PEDIDOS ";
		sql += "FROM( ";
		sql += "SELECT PED.IDPEDIDO, PED.FECHA, PED.IDUSUARIO, PRODPED.NOMPRODUCTO, PRODPED.NOMRESTAURANTE ";
		sql += "FROM PEDIDO PED INNER JOIN PRODUCTOPEDIDO PRODPED ";
		sql += "ON PED.IDPEDIDO = PRODPED.IDPEDIDO ";
		sql += "WHERE NOMRESTAURANTE LIKE '" + restaurante + "' AND IDUSUARIO LIKE '" + pIdUsuario + "' ";
		sql += "AND PED.FECHA > TO_DATE('" + fechaInicial + "','DD-MM-YYYY') ";
		sql += "AND PED.FECHA < TO_DATE('" + fechaFinal + "','DD-MM-YYYY') ORDER BY PRODPED.NOMPRODUCTO) P ";
		sql += "GROUP BY P.IDUSUARIO) A INNER JOIN USUARIO ";
		sql += "ON A.IDUSUARIO = USUARIO.IDENTIFICACION) ";
		sql += "WHERE CANTIDAD_PEDIDOS > 0 ";
		
		System.out.println("SENTENCIA: " + sql);
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			String idUsuario = rs.getString("IDUSUARIO");
			int cantidadPedidos = rs.getInt("CANTIDAD_PEDIDOS");
			String nombre = rs.getString("NOMBRE");
			String correo = rs.getString("CORREO");
			String rol = rs.getString("ROL");
			
			RFC9Simple nuevo = new RFC9Simple(idUsuario, cantidadPedidos, nombre, correo, rol);
			respuesta.add(nuevo);
		}
		
		return respuesta;
	}
	
	
}
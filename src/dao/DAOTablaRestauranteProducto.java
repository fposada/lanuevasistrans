package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import vos.Producto;
import vos.RestauranteProducto;

public class DAOTablaRestauranteProducto {

	/**
	 * Arraylits de recursos que se usan para la ejecucion de sentencias SQL
	 */
	private ArrayList<Object> recursos;

	/**
	 * Atributo que genera la conexion a la base de datos
	 */
	private Connection conn;

	/**
	 * Metodo constructor que crea DAOTablaIngredientes
	 * <b>post: </b> Crea la instancia del DAO e inicializa el Arraylist de recursos
	 */
	public DAOTablaRestauranteProducto() {
		recursos = new ArrayList<Object>();
	}

	/**
	 * Metodo que cierra todos los recursos que estan enel arreglo de recursos
	 * <b>post: </b> Todos los recurso del arreglo de recursos han sido cerrados
	 */
	public void cerrarRecursos() {
		for(Object ob : recursos){
			if(ob instanceof PreparedStatement)
				try {
					((PreparedStatement) ob).close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
		}
	}

	/**
	 * Metodo que inicializa la connection del DAO a la base de datos con la conexion que entra como parametro.
	 * @param con  - connection a la base de datos
	 */
	public void setConn(Connection con){
		this.conn = con;
	}

	public ArrayList<RestauranteProducto> darProductosPorRestaurante(String nom) throws Exception, SQLException
	{
		ArrayList<RestauranteProducto> Usuarioz = new ArrayList<RestauranteProducto>();

		String sql = "SELECT * FROM RESTAURANTEPRODUCTO";
		sql += " WHERE NOMRESTAURANTE = '" + nom +"'";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		ResultSet rs = prepStmt.executeQuery();

		while (rs.next()) {
			String nomP=rs.getString("NOMPRODUCTO");
			String nomR=rs.getString("NOMRESTAURANTE");
			double costo=rs.getDouble("COSTOPROD");
			double pre=rs.getDouble("PRECIO");
			int un=rs.getInt("UNIDADESDISPONIBLES");
			int cant = rs.getInt("CANTIDADMAXIMA");
			Usuarioz.add(new RestauranteProducto(nomP, nomR, costo, pre,un, cant));
		}
		return Usuarioz;
	}

	public RestauranteProducto darRestauranteProducto(String nomRe, String nomPr) throws Exception, SQLException
	{
		RestauranteProducto resPro = null;

		String sql = "SELECT * FROM RESTAURANTEPRODUCTO";
		sql += " WHERE NOMRESTAURANTE = '" + nomRe+"'";
		sql += " AND NOMPRODUCTO = '" + nomPr+"'";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		ResultSet rs = prepStmt.executeQuery();

		while (rs.next()) {
			String nomP=rs.getString("NOMPRODUCTO");
			String nomR=rs.getString("NOMRESTAURANTE");
			double costo=rs.getDouble("COSTOPROD");
			double pre=rs.getDouble("PRECIO");
			int un=rs.getInt("UNIDADESDISPONIBLES");
			int cant = rs.getInt("CANTIDADMAXIMA");
			resPro=new RestauranteProducto(nomP, nomR, costo, pre,un, cant);
		}
		return resPro;
	}

	public ArrayList<RestauranteProducto> darRestaurantesPorProducto(String nom) throws Exception, SQLException
	{
		ArrayList<RestauranteProducto> Usuarioz = new ArrayList<RestauranteProducto>();

		String sql = "SELECT * FROM RESTAURANTEPRODUCTO";
		sql += " WHERE NOMPRODUCTO = '" + nom +"'";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		ResultSet rs = prepStmt.executeQuery();

		while (rs.next()) {
			String nomP=rs.getString("NOMPRODUCTO");
			String nomR=rs.getString("NOMRESTAURANTE");
			double costo=rs.getDouble("COSTOPROD");
			double pre=rs.getDouble("PRECIO");
			int un=rs.getInt("UNIDADESDISPONIBLES");
			int cant = rs.getInt("CANTIDADMAXIMA");
			Usuarioz.add(new RestauranteProducto(nomP, nomR, costo, pre,un, cant));
		}
		return Usuarioz;
	}
	
	public ArrayList<Producto> darMenusPorRestaurante(String nom) throws Exception, SQLException
	{
		ArrayList<Producto> productoz = new ArrayList<Producto>();

		String sql = "SELECT * FROM PRODUCTO INNER JOIN RESTAURANTEPRODUCTO ON NOMBRE=NOMPRODUCTO"
		+" WHERE CATEGORIA='Menu' AND NOMRESTAURANTE='"+nom+"'";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		ResultSet rs = prepStmt.executeQuery();

		while (rs.next()) {
			String name = rs.getString("NOMBRE");
			String desc=rs.getString("DESCRIPCION");
			String descT=rs.getString("DESCTRADUCIDA");
			Integer tie=rs.getInt("TIEMPOPREP");
			String tip=rs.getString("TIPO");
			String cat=rs.getString("CATEGORIA");
			productoz.add(new Producto(name, desc, descT, tie, tip, cat));
		}
		return productoz;
	}
	
	public ArrayList<RestauranteProducto> darProductosNoMenPorRestaurante(String nom) throws Exception, SQLException
	{
		ArrayList<RestauranteProducto> Usuarioz = new ArrayList<RestauranteProducto>();

		String sql = "SELECT * FROM PRODUCTO INNER JOIN RESTAURANTEPRODUCTO ON NOMBRE=NOMPRODUCTO WHERE CATEGORIA<>'Menu'";
		sql += " AND NOMRESTAURANTE = '" + nom +"'";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		ResultSet rs = prepStmt.executeQuery();

		while (rs.next()) {
			String nomP=rs.getString("NOMPRODUCTO");
			String nomR=rs.getString("NOMRESTAURANTE");
			double costo=rs.getDouble("COSTOPROD");
			double pre=rs.getDouble("PRECIO");
			int un=rs.getInt("UNIDADESDISPONIBLES");
			int cant = rs.getInt("CANTIDADMAXIMA");
			Usuarioz.add(new RestauranteProducto(nomP, nomR, costo, pre,un, cant));
		}
		return Usuarioz;
	}
	
	



	public void addRestauranteProducto(RestauranteProducto rest) throws SQLException, Exception {

		String sql = "INSERT INTO RESTAURANTEPRODUCTO VALUES ('";
		sql += rest.getNomProducto() + "','";
		sql += rest.getNomRestaurante() + "',";
		sql += rest.getCostoProd() + ",";
		sql += rest.getPrecio() + ",";
		sql += rest.getUnidadesDisponibles() + ",";
		sql += rest.getCantidadMaxima() + ")";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		prepStmt.executeQuery();

	}

	public void updateRestauranteProducto(RestauranteProducto rest) throws SQLException, Exception {

		String sql = "UPDATE RESTAURANTEPRODUCTO SET COSTOPROD=";
		sql += rest.getCostoProd()+", PRECIO=";
		sql += rest.getPrecio()+", UNIDADESDISPONIBLES=";
		sql += rest.getUnidadesDisponibles();
		sql += "WHERE NOMPRODUCTO = '"+rest.getNomProducto()+"' AND NOMRESTAURANTE = '"+rest.getNomRestaurante()+"'";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		prepStmt.executeQuery();
	}
	
	public void updateCantidad(String r) throws SQLException, Exception {

		String sql = "UPDATE RESTAURANTEPRODUCTO SET UNIDADESDISPONIBLES = CANTIDADMAXIMA";
		sql += " WHERE NOMRESTAURANTE = '"+r+"'";
		System.err.println(sql);
		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
	    prepStmt.executeQuery();
			
		
	}

	public void deleteRestauranteProducto(RestauranteProducto rest) throws SQLException, Exception {

		String sql = "DELETE FROM RestauranteProducto";
		sql += " WHERE NOMRESTAURANTE = '" + rest.getNomRestaurante()+"'";
		sql += " AND NOMPRODUCTO = '" + rest.getNomProducto()+"'";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		prepStmt.executeQuery();
	}
	
	public void restarProducto(String nomProducto) throws SQLException, Exception{
		String sql = "UPDATE RESTAURANTEPRODUCTO SET UNIDADESDISPONIBLES = (UNIDADESDISPONIBLES-1) ";
		sql += "WHERE NOMPRODUCTO = '" + nomProducto + "'";
		//sql += "\n COMMIT;";
		
		System.out.println(sql);
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.executeQuery();
	}
	
	public void sumarProducto(String nomProducto) throws SQLException, Exception{
		String sql = "UPDATE RESTAURANTEPRODUCTO SET UNIDADESDISPONIBLES = (UNIDADESDISPONIBLES+1) ";
		sql += "WHERE NOMPRODUCTO = '" + nomProducto + "'";
		//sql += "\n COMMIT;";
		
		System.out.println(sql);
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.executeQuery();
	}

}


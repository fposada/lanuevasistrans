package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import vos.Usuario;

public class DAOTablaUsuarios {
	
	/**
	 * Arraylits de recursos que se usan para la ejecucion de sentencias SQL
	 */
	private ArrayList<Object> recursos;

	/**
	 * Atributo que genera la conexion a la base de datos
	 */
	private Connection conn;

	/**
	 * Metodo constructor que crea DAOTablaUsuarios
	 * <b>post: </b> Crea la instancia del DAO e inicializa el Arraylist de recursos
	 */
	public DAOTablaUsuarios() {
		recursos = new ArrayList<Object>();
	}

	/**
	 * Metodo que cierra todos los recursos que estan enel arreglo de recursos
	 * <b>post: </b> Todos los recurso del arreglo de recursos han sido cerrados
	 */
	public void cerrarRecursos() {
		for(Object ob : recursos){
			if(ob instanceof PreparedStatement)
				try {
					((PreparedStatement) ob).close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
		}
	}

	/**
	 * Metodo que inicializa la connection del DAO a la base de datos con la conexion que entra como parametro.
	 * @param con  - connection a la base de datos
	 */
	public void setConn(Connection con){
		this.conn = con;
	}
	
	
	public ArrayList<Usuario> darUsuarios() throws SQLException, Exception {
		ArrayList<Usuario> Usuarioz = new ArrayList<Usuario>();

		String sql = "SELECT * FROM USUARIO";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		ResultSet rs = prepStmt.executeQuery();
		

		while (rs.next()) {
			String pName=rs.getString("NOMBRE");

			System.out.println("Sigue el usuario "+pName);
			String pId=rs.getString("IDENTIFICACION");
			String pCorreo=rs.getString("CORREO");
			String pRol=rs.getString("ROL");
			Usuarioz.add(new Usuario(pName, pId, pCorreo, pRol));
		}
		return Usuarioz;
	}
	
	public ArrayList<Usuario> darUsuariosNoRest(String fechaIn, String fechaFi, String nomR) throws SQLException, Exception {
		ArrayList<Usuario> Usuarioz = new ArrayList<Usuario>();
		System.out.println(fechaIn);
		System.out.println(fechaFi);
		System.out.println(nomR);
		
		String sql1="(SELECT USUARIO.NOMBRE, USUARIO.IDENTIFICACION, USUARIO.CORREO, USUARIO.ROL FROM PEDIDO INNER JOIN USUARIO ON PEDIDO.IDUSUARIO=USUARIO.IDENTIFICACION INNER JOIN PRODUCTOPEDIDO ON PEDIDO.IDPEDIDO=PRODUCTOPEDIDO.IDPEDIDO WHERE PEDIDO.FECHA > to_date('"+fechaIn+"','DD-MM-YYYY') AND PEDIDO.FECHA < TO_DATE('"
		+fechaFi+"','DD-MM-YYYY')) ";
		String sql2="MINUS ";
		String sql3="(SELECT USUARIO.NOMBRE, USUARIO.IDENTIFICACION, USUARIO.CORREO, USUARIO.ROL FROM PEDIDO INNER JOIN USUARIO ON PEDIDO.IDUSUARIO=USUARIO.IDENTIFICACION INNER JOIN PRODUCTOPEDIDO ON PEDIDO.IDPEDIDO=PRODUCTOPEDIDO.IDPEDIDO WHERE PEDIDO.FECHA > to_date('"+fechaIn+"','DD-MM-YYYY') AND PEDIDO.FECHA < TO_DATE('"
				+fechaFi+"','DD-MM-YYYY') AND PRODUCTOPEDIDO.NOMRESTAURANTE ='"+nomR+"')";
		

		String sql=sql1+sql2+sql3;
		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		long startTime = System.currentTimeMillis();
		ResultSet rs = prepStmt.executeQuery();
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		System.out.println("Tiempo transcurrido (ms): "+elapsedTime);
		System.out.println(sql);

		
		System.out.println("Usuarios que fueron a RotondAndes y no pidieron en "+nomR);
		while (rs.next()) {
			String pName=rs.getString("NOMBRE");

			String pId=rs.getString("IDENTIFICACION");
			String pCorreo=rs.getString("CORREO");
			String pRol=rs.getString("ROL");
			Usuarioz.add(new Usuario(pName, pId, pCorreo, pRol));
		}
		return Usuarioz;
	}
	
	public ArrayList<Usuario> darUsuariosNoRestPro(String fechaIn, String fechaFi, String nomR, String nomP) throws SQLException, Exception {
		ArrayList<Usuario> Usuarioz = new ArrayList<Usuario>();

		String sql = "(SELECT USUARIO.NOMBRE, USUARIO.IDENTIFICACION, USUARIO.CORREO, USUARIO.ROL\n" + 
				"FROM PEDIDO INNER JOIN USUARIO ON PEDIDO.IDUSUARIO=USUARIO.IDENTIFICACION INNER JOIN PRODUCTOPEDIDO ON PEDIDO.IDPEDIDO=PRODUCTOPEDIDO.IDPEDIDO\n" + 
				"WHERE PEDIDO.FECHA > to_date('"+fechaIn+"','DD-MM-YYYY') AND PEDIDO.FECHA < TO_DATE('"+fechaFi+"','DD-MM-YYYY'))\n" + 
				"MINUS\n" + 
				"(SELECT USUARIO.NOMBRE, USUARIO.IDENTIFICACION, USUARIO.CORREO, USUARIO.ROL\n" + 
				"FROM PEDIDO INNER JOIN USUARIO ON PEDIDO.IDUSUARIO=USUARIO.IDENTIFICACION INNER JOIN PRODUCTOPEDIDO ON PEDIDO.IDPEDIDO=PRODUCTOPEDIDO.IDPEDIDO\n" + 
				"WHERE PEDIDO.FECHA > to_date('"+fechaIn+"','DD-MM-YYYY') AND PEDIDO.FECHA < TO_DATE('"+fechaFi+"','DD-MM-YYYY') AND PRODUCTOPEDIDO.NOMRESTAURANTE='"+nomR+
				"' AND PRODUCTOPEDIDO.NOMPRODUCTO = '"+nomP+"')";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		long startTime = System.currentTimeMillis();
		ResultSet rs = prepStmt.executeQuery();
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		System.out.println(sql);

		System.out.println("Tiempo transcurrido (ms): "+elapsedTime);
		

		System.out.println("Usuarios que fueron a RotondAndes y no pidieron el producto "+nomP+" en "+nomR);
		while (rs.next()) {


			String pName=rs.getString("NOMBRE");
			String pId=rs.getString("IDENTIFICACION");
			String pCorreo=rs.getString("CORREO");
			String pRol=rs.getString("ROL");
			Usuarioz.add(new Usuario(pName, pId, pCorreo, pRol));
		}
		return Usuarioz;
	}
	
	public ArrayList<Usuario> darUsuariosNoRestTipo(String fechaIn, String fechaFi, String nomR, String tipo) throws SQLException, Exception {
		ArrayList<Usuario> Usuarioz = new ArrayList<Usuario>();

		String sql = "(SELECT USUARIO.NOMBRE, USUARIO.IDENTIFICACION, USUARIO.CORREO, USUARIO.ROL\n" + 
				"FROM PEDIDO INNER JOIN USUARIO ON PEDIDO.IDUSUARIO=USUARIO.IDENTIFICACION INNER JOIN PRODUCTOPEDIDO ON PEDIDO.IDPEDIDO=PRODUCTOPEDIDO.IDPEDIDO INNER JOIN PRODUCTO ON PRODUCTOPEDIDO.NOMPRODUCTO=PRODUCTO.NOMBRE\n" + 
				"WHERE PEDIDO.FECHA > to_date('"+fechaIn+"','DD-MM-YYYY') AND PEDIDO.FECHA < TO_DATE('"+fechaFi+"','DD-MM-YYYY'))\n" + 
				"MINUS\n" + 
				"(SELECT USUARIO.NOMBRE, USUARIO.IDENTIFICACION, USUARIO.CORREO, USUARIO.ROL\n" + 
				"FROM PEDIDO INNER JOIN USUARIO ON PEDIDO.IDUSUARIO=USUARIO.IDENTIFICACION INNER JOIN PRODUCTOPEDIDO ON PEDIDO.IDPEDIDO=PRODUCTOPEDIDO.IDPEDIDO INNER JOIN PRODUCTO ON PRODUCTOPEDIDO.NOMPRODUCTO=PRODUCTO.NOMBRE\n" + 
				"WHERE PEDIDO.FECHA > to_date('"+fechaIn+"','DD-MM-YYYY') AND PEDIDO.FECHA < TO_DATE('"+fechaFi+"','DD-MM-YYYY') AND PRODUCTOPEDIDO.NOMRESTAURANTE='"+nomR+
				"' AND PRODUCTO.TIPO = '"+tipo+"')";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		long startTime = System.currentTimeMillis();
		ResultSet rs = prepStmt.executeQuery();
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		System.out.println("Tiempo transcurrido (ms): "+elapsedTime);
		

		System.out.println(sql);
		System.out.println("Usuarios que fueron a RotondAndes y no pidieron productos del tipo "+tipo+" en "+nomR);
		while (rs.next()) {


			String pName=rs.getString("NOMBRE");
			String pId=rs.getString("IDENTIFICACION");
			String pCorreo=rs.getString("CORREO");
			String pRol=rs.getString("ROL");
			Usuarioz.add(new Usuario(pName, pId, pCorreo, pRol));
		}
		return Usuarioz;
	}
	
	public ArrayList<Usuario> darClientesBuenos() throws SQLException, Exception {
		ArrayList<Usuario> Usuarioz = new ArrayList<Usuario>();

		String sql = "(SELECT Q.NOMBRE, Q.CORREO, Q.IDENTIFICACION\n" + 
				"FROM USUARIO Q INNER JOIN PEDIDO ON IDUSUARIO=IDENTIFICACION\n" + 
				"WHERE IDPEDIDO IN(\n" + 
				"SELECT IDPEDIDO\n" + 
				"FROM(\n" + 
				"SELECT *\n" + 
				"FROM PRODUCTOPEDIDO\n" + 
				"WHERE IDPEDIDO IN(\n" + 
				"SELECT PEDIDO.IDPEDIDO\n" + 
				"FROM PEDIDO INNER JOIN USUARIO ON USUARIO.IDENTIFICACION=PEDIDO.IDUSUARIO\n" + 
				"WHERE IDENTIFICACION IN(\n" + 
				"SELECT X.IDENTIFICACION\n" + 
				"FROM PRODUCTOPEDIDO INNER JOIN PEDIDO ON PRODUCTOPEDIDO.IDPEDIDO = PEDIDO.IDPEDIDO INNER JOIN USUARIO X ON USUARIO.IDENTIFICACION=PEDIDO.IDUSUARIO INNER JOIN RESTAURANTEPRODUCTO ON RESTAURANTEPRODUCTO.NOMPRODUCTO = PRODUCTOPEDIDO.NOMPRODUCTO\n" + 
				"WHERE PRECIO>1105000)))))\n" + 
				"UNION\n" + 
				"(select Z.IDENTIFICACION, Z.NOMBRE, Z.CORREO \n" + 
				"FROM(\n" + 
				"select idUsuario, cuenta\n" + 
				"from (select idUsuario, Count (*) as cuenta\n" + 
				"from usuario inner join pedido on usuario.IDENTIFICACION=pedido.IDUSUARIO\n" + 
				"group by idUsuario)\n" + 
				"where cuenta=(\n" + 
				"select max(cuenta)\n" + 
				"from(select idUsuario, Count (*) as cuenta\n" + 
				"from usuario inner join pedido on usuario.IDENTIFICACION=pedido.IDUSUARIO\n" + 
				"group by idUsuario))) JOIN USUARIO Z ON Z.IDENTIFICACION=idUsuario)\n" + 
				"UNION\n" + 
				"(\n" + 
				"SELECT Q.NOMBRE, Q.CORREO, Q.IDENTIFICACION\n" + 
				"FROM USUARIO Q\n" + 
				"WHERE Q.IDENTIFICACION NOT IN(\n" + 
				"SELECT X.IDENTIFICACION\n" + 
				"FROM PRODUCTOPEDIDO INNER JOIN PEDIDO ON PRODUCTOPEDIDO.IDPEDIDO = PEDIDO.IDPEDIDO INNER JOIN USUARIO X ON X.IDENTIFICACION=PEDIDO.IDUSUARIO \n" + 
				"INNER JOIN PRODUCTO ON PRODUCTO.NOMBRE = PRODUCTOPEDIDO.NOMPRODUCTO\n" + 
				"WHERE PRODUCTO.CATEGORIA='Menu')\n" + 
				")";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		long startTime = System.currentTimeMillis();
		ResultSet rs = prepStmt.executeQuery();
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		System.out.println("Tiempo transcurrido (ms): "+elapsedTime);
		

		System.out.println("Clientes Buenos");
		int i=0;
		while (rs.next()&&i<100) {


			i++;
			String pName=rs.getString("NOMBRE");
			String pId=rs.getString("IDENTIFICACION");
			String pCorreo=rs.getString("CORREO");
			String pRol="UsuarioCliente";
			Usuarioz.add(new Usuario(pName, pId, pCorreo, pRol));
		}
		return Usuarioz;
	}
	
	
	
	public Usuario buscarUsuarioPorId(String id) throws SQLException, Exception 
	{
		Usuario uzer = null;

		String sql = "SELECT * FROM USUARIO WHERE IDENTIFICACION = '" + id+"'";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		ResultSet rs = prepStmt.executeQuery();

		if(rs.next()) {
			String pName=rs.getString("NOMBRE");
			String pId=rs.getString("IDENTIFICACION");
			String pCorreo=rs.getString("CORREO");
			String pRol=rs.getString("ROL");
			uzer = new Usuario(pName, pId, pCorreo, pRol);

		}

		return uzer;
	}	
	
	
	
	

	public void addUsuario(Usuario us) throws SQLException, Exception {
		
		Usuario testeo=buscarUsuarioPorId(us.getIdentificacion());
		if(testeo!=null)
			throw new Exception("Un usuario con la identificacion "+us.getIdentificacion()+" ya existe.");

		String sql = "INSERT INTO USUARIO VALUES ('";
		sql += us.getNombre() + "','";
		sql += us.getIdentificacion() + "','";
		sql += us.getCorreo() + "','";
		sql += us.getRol() + "')";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		prepStmt.executeQuery();
		

	}
	
	public void updateUsuario(Usuario us) throws SQLException, Exception {

		String sql = "UPDATE USUARIO SET NOMBRE='";
		sql += us.getNombre() + "', CORREO='";
		sql += us.getCorreo() + "',ROL='";
		sql += us.getRol() + "'";
		sql += " WHERE IDENTIFICACION = '" + us.getIdentificacion()+"'";


		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		prepStmt.executeQuery();
	}
	
	public void deleteUsuario(Usuario us) throws SQLException, Exception {

		String sql = "DELETE FROM USUARIO";
		sql += " WHERE IDENTIFICACION = '" + us.getIdentificacion()+"'";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		prepStmt.executeQuery();
	}


}
